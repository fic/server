FROM golang:1-alpine as gobuild

RUN apk add --no-cache git

WORKDIR /go/src/srs.epita.fr/fic-server/

RUN apk add --no-cache binutils-gold build-base

COPY go.mod go.sum ./
COPY settings settings/
COPY libfic ./libfic/
COPY admin ./admin/
COPY repochecker ./repochecker/

RUN go get -d -v ./admin && \
    go build -v -o admin/admin ./admin && \
    go build -v -buildmode=plugin -o repochecker/epita-rules.so ./repochecker/epita && \
    go build -v -buildmode=plugin -o repochecker/file-inspector.so ./repochecker/file-inspector && \
    go build -v -buildmode=plugin -o repochecker/grammalecte-rules.so ./repochecker/grammalecte && \
    go build -v -buildmode=plugin -o repochecker/videos-rules.so ./repochecker/videos


FROM alpine:3.20

RUN apk add --no-cache \
        ca-certificates \
        git \
        git-lfs \
        openssh-client-default \
        openssl

EXPOSE 8081

WORKDIR /srv

ENTRYPOINT ["/srv/admin", "-bind=:8081", "-baseurl=/admin/"]

COPY --from=gobuild /go/src/srs.epita.fr/fic-server/admin/admin /srv/admin
COPY --from=gobuild /go/src/srs.epita.fr/fic-server/repochecker/epita-rules.so /srv/epita-rules.so
COPY --from=gobuild /go/src/srs.epita.fr/fic-server/repochecker/file-inspector.so /usr/lib/file-inspector.so
COPY --from=gobuild /go/src/srs.epita.fr/fic-server/repochecker/grammalecte-rules.so /usr/lib/grammalecte-rules.so
COPY --from=gobuild /go/src/srs.epita.fr/fic-server/repochecker/videos-rules.so /usr/lib/videos-rules.so
