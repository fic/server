package api

import (
	"archive/zip"
	"encoding/json"
	"net/http"
	"path"

	"srs.epita.fr/fic-server/libfic"
	"srs.epita.fr/fic-server/settings"

	"github.com/gin-gonic/gin"
)

func declareExportRoutes(router *gin.RouterGroup) {
	router.GET("/archive.zip", func(c *gin.Context) {
		challengeinfo, err := GetChallengeInfo()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		my, err := fic.MyJSONTeam(nil, true)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		s, err := settings.ReadSettings(path.Join(settings.SettingsDir, settings.SettingsFile))
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}
		s.End = nil
		s.NextChangeTime = nil
		s.DelegatedQA = []string{}

		teams, err := fic.ExportTeams(false)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		themes, err := fic.ExportThemes()
		if err != nil {
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": err.Error()})
			return
		}

		c.Writer.WriteHeader(http.StatusOK)
		c.Header("Content-Disposition", "attachment; filename=archive.zip")
		c.Header("Content-Type", "application/zip")

		w := zip.NewWriter(c.Writer)

		// challenge.json
		f, err := w.Create("challenge.json")
		if err == nil {
			json.NewEncoder(f).Encode(challengeinfo)
		}

		// my.json
		f, err = w.Create("my.json")
		if err == nil {
			json.NewEncoder(f).Encode(my)
		}

		// settings.json
		f, err = w.Create("settings.json")
		if err == nil {
			json.NewEncoder(f).Encode(s)
		}

		// teams.json
		f, err = w.Create("teams.json")
		if err == nil {
			json.NewEncoder(f).Encode(teams)
		}

		// themes.json
		f, err = w.Create("themes.json")
		if err == nil {
			json.NewEncoder(f).Encode(themes)
		}

		w.Close()
	})
}
