package api

import (
	"log"
	"net/http"
	"strconv"

	"srs.epita.fr/fic-server/libfic"

	"github.com/gin-gonic/gin"
)

func declareQARoutes(router *gin.RouterGroup) {
	router.POST("/qa/", importExerciceQA)

	apiQARoutes := router.Group("/qa/:qid")
	apiQARoutes.Use(QAHandler)
	apiQARoutes.POST("/comments", importQAComment)
}

func QAHandler(c *gin.Context) {
	qid, err := strconv.ParseInt(string(c.Params.ByName("qid")), 10, 64)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Invalid QA identifier"})
		return
	}

	qa, err := fic.GetQAQuery(qid)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": "QA query not found"})
		return
	}

	c.Set("qa-query", qa)

	c.Next()
}

func importExerciceQA(c *gin.Context) {
	// Create a new query
	var uq fic.QAQuery
	err := c.ShouldBindJSON(&uq)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
		return
	}

	var exercice *fic.Exercice
	if uq.IdExercice == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "id_exercice not filled"})
		return
	} else if exercice, err = fic.GetExercice(uq.IdExercice); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Unable to find requested exercice"})
		return
	}

	if len(uq.State) == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "State not filled"})
		return
	}

	if len(uq.Subject) == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Subject not filled"})
		return
	}

	if qa, err := exercice.NewQAQuery(uq.Subject, uq.IdTeam, uq.User, uq.State, nil); err != nil {
		log.Println("Unable to importExerciceQA:", err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "An error occurs during query creation."})
		return
	} else {
		qa.Creation = uq.Creation
		qa.Solved = uq.Solved
		qa.Closed = uq.Closed

		_, err = qa.Update()
		if err != nil {
			log.Println("Unable to update in importExerciceQA:", err.Error())
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "An error occurs during query updating."})
			return
		}

		c.JSON(http.StatusOK, qa)
	}
}

func importQAComment(c *gin.Context) {
	query := c.MustGet("qa-query").(*fic.QAQuery)

	// Create a new query
	var uc fic.QAComment
	err := c.ShouldBindJSON(&uc)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": err.Error()})
		return
	}

	if len(uc.Content) == 0 {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Empty comment"})
		return
	}

	if qac, err := query.AddComment(uc.Content, uc.IdTeam, uc.User); err != nil {
		log.Println("Unable to AddComment in importQAComment:", err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "An error occurs during comment creation."})
		return
	} else {
		qac.Date = uc.Date

		_, err = qac.Update()
		if err != nil {
			log.Println("Unable to Update comment in importQAComment")
			c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": "An error occurs during comment creation."})
			return
		}

		c.JSON(http.StatusOK, qac)
	}
}
