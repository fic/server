package api

import (
	"github.com/gin-gonic/gin"
)

func DeclareRoutes(router *gin.RouterGroup) {
	apiRoutes := router.Group("/api")

	declareCertificateRoutes(apiRoutes)
	declareClaimsRoutes(apiRoutes)
	declareEventsRoutes(apiRoutes)
	declareExercicesRoutes(apiRoutes)
	declareExportRoutes(apiRoutes)
	declareFilesGlobalRoutes(apiRoutes)
	declareFilesRoutes(apiRoutes)
	declareGlobalExercicesRoutes(apiRoutes)
	declareHealthRoutes(apiRoutes)
	declareMonitorRoutes(apiRoutes)
	declarePasswordRoutes(apiRoutes)
	declarePublicRoutes(apiRoutes)
	declareQARoutes(apiRoutes)
	declareRepositoriesRoutes(apiRoutes)
	declareTeamsRoutes(apiRoutes)
	declareThemesRoutes(apiRoutes)
	declareSettingsRoutes(apiRoutes)
	declareSyncRoutes(apiRoutes)
	DeclareVersionRoutes(apiRoutes)
}
