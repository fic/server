package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func DeclareVersionRoutes(router *gin.RouterGroup) {
	router.GET("/version", showVersion)
}

func showVersion(c *gin.Context) {
	c.JSON(http.StatusOK, gin.H{"version": 1.0})
}
