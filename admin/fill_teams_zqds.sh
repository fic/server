#!/bin/sh

BASEURL="http://127.0.0.1:8081/admin"

EVENTID=6109ae5acbb7b36b789c9330
BASEURL_ZQDS="https://api.well-played.gg"

curl -s -H 'accept: */*' "${BASEURL_ZQDS}/teams?event_id=${EVENTID}&size=100" | jq --compact-output .content[] | while read TEAMOBJ; do
    curl -s -d @- "${BASEURL}/api/teams/" <<EOF
{
  "name": $(echo "${TEAMOBJ}" | jq .name),
  "external_id": $(echo "${TEAMOBJ}" | jq .id)
}
EOF
done
