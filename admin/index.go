package main

const indextpl = `<!DOCTYPE html>
<html ng-app="FICApp">
  <head>
    <meta charset="utf-8">
    <title>Challenge Forensic - Administration</title>
    <link href="{{.urlbase}}css/bootstrap.min.css" type="text/css" rel="stylesheet">
    <link href="{{.urlbase}}css/glyphicon.css" type="text/css" rel="stylesheet" media="screen">
    <style>
    .cksum {
      overflow-x: hidden;
      text-overflow: ellipsis;
      max-width: 100%;
      display: inline-block;
      vertical-align: middle;
      word-wrap: normal;
      white-space: nowrap;
    }
    .bg-mfound {
      background-color: #7bcfd0 !important;
    }
    .bg-ffound {
      background-color: #7bdfc0 !important;
    }
    .bg-wchoices {
      background-color: #c07bdf !important;
    }
    .table th.frotated {
        border: 0;
    }
    .table th.rotated {
        height: 100px;
        width: 40px;
        min-width: 40px;
        max-width: 40px;
        position: relative;
        vertical-align: bottom;
        padding: 0;
        font-size: 12px;
        line-height: 0.9;
        border: 0;
    }

    th.rotated > div {
        position: relative;
        top: 0px;
        left: -50px;
        height: 100%;
        transform: skew(45deg,0deg);
        overflow: hidden;
        border: 1px solid #000;
    }
    th.rotated div a {
        transform: skew(-45deg,0deg) rotate(45deg);
        position: absolute;
        bottom: 40px;
        left: -35px;
        display: inline-block;
        width: 110px;
        text-align: left;
        text-overflow: ellipsis;
    }
    .col img {
       max-width: 100%;
    }
    .circle-anim {
       z-index:1;
       border: black 1px solid;
       border-radius: .5em;
       margin-top: .4em;
       margin-left: .5em;
       height: 1em;
       width: 1em;
       transition: transform ease-in .7s;
       transform: scale(1);
    }
    .circle-anim.play {
       transform: scale(250);
       opacity:0;
    }
    </style>
    <base href="{{.urlbase}}">
    <script src="js/d3.v3.min.js"></script>
  </head>
  <body class="bg-light text-dark">
    <nav class="navbar sticky-top navbar-expand-lg navbar-dark text-light" ng-class="{'bg-dark': settings.wip, 'bg-danger': !settings.wip}">
      <a class="navbar-brand" href=".">
	<img alt="FIC" src="img/fic.png" style="height: 30px">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#adminMenu" aria-controls="adminMenu" aria-expanded="false" aria-label="Toggle navigation">
	<span class="navbar-toggler-icon"></span>
      </button>

      <div class="collapse navbar-collapse" id="adminMenu">
	<ul class="navbar-nav mr-auto">
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/teams')}"><a class="nav-link" href="teams">&Eacute;quipes</a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/pki')}"><a class="nav-link" href="pki">PKI</a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/themes')}"><a class="nav-link" href="themes">Thèmes</a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/exercices')}"><a class="nav-link" href="exercices">Exercices</a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/public')}"><a class="nav-link" href="public/0">Public</a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/events')}"><a class="nav-link" href="events">&Eacute;vénements</a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/claims')}"><a class="nav-link" href="claims" ng-controller="ClaimsTinyListController">
	      Tâches
	      <span class="badge badge-{{ "{{ priorities[myClaimsMaxLevel] }}" }}" ng-show="myClaims" title="Tâches qui me sont assignées">{{ "{{ myClaims }}" }}</span>
	      <span class="badge badge-{{ "{{ priorities[newClaimsMaxLevel] }}" }}" ng-show="newClaims" title="Nouvelles tâches en attente d'attribution">{{ "{{ newClaims }}" }}</span>
	  </a></li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/sync') || $location.path().startsWith('/repositories')}">
              <a class="nav-link" href="sync" ng-show="settings.wip">
                  Synchronisation
              </a>
          </li>
	  <li class="nav-item" ng-class="{'active': $location.path().startsWith('/settings')}">
              <a class="nav-link" href="settings">
                  Paramètres
              </a>
          </li>
          <span class="d-flex flex-column justify-content-center" ng-show="!settings.wip" ng-cloak>
              <span class="badge badge-light p-1">
                  prod
              </span>
          </span>
	</ul>
      </div>

      <span id="clock" class="navbar-text" ng-controller="CountdownController" ng-cloak>
        <div style="position: absolute;">
          <div style="position: absolute;" id="circle1" class="circle-anim border-danger"></div>
          <div style="position: absolute;" id="circle2" class="circle-anim border-info"></div>
        </div>
        <button type="button" class="mr-2 btn btn-sm" ng-class="{'btn-info':staticFilesNeedUpdate,'btn-secondary':!staticFilesNeedUpdate}" ng-click="regenerateStaticFiles()" ng-disabled="staticRegenerationInProgress">
          <span class="glyphicon glyphicon-refresh" aria-hidden="true" title="Regénérer les fichiers statiques" ng-show="!staticRegenerationInProgress"></span>
          <div class="spinner-border spinner-border-sm" role="status" ng-show="staticRegenerationInProgress">
            <span class="sr-only">Loading...</span>
          </div>
          <span ng-if="staticFilesNeedUpdate"> {{ "{{ staticFilesNeedUpdate }}" }}</span>
        </button>
	<span ng-show="startIn > 0">
	  Démarrage dans :
          <span>{{"{{ startIn }}"}}</span>"
          <span class="point">|</span>
	</span>
	<span ng-show="settings && settings.end > 0">
          <span id="hours">{{"{{ time.hours | time }}"}}</span>
          <span class="point">:</span>
          <span id="min">{{"{{ time.minutes | time }}"}}</span>
          <span class="point">:</span>
          <span id="sec">{{"{{ time.seconds | time }}"}}</span>
        </span>
      </span>
    </nav>

    <div class="progress" style="background-color: #4eaee6; height: 3px; border-radius: 0;">
      <div class="progress-bar bg-secondary" role="progressbar" style="width: {{ "{{timeProgression * 100}}" }}%"></div>
    </div>

    <div class="container mt-1" ng-view></div>

    <div style="position: fixed; top: 60px; right: 0; z-index: 10; min-width: 30vw;">
      <toast ng-repeat="toast in toasts" yes-no="toast.yesFunc || toast.noFunc" onyes="toast.yesFunc" onno="toast.noFunc" date="toast.date" msg="toast.msg" timeout="toast.timeout" title="toast.title" variant="toast.variant"></toast>
    </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/angular.min.js"></script>
    <script src="js/angular-resource.min.js"></script>
    <script src="js/angular-route.min.js"></script>
    <script src="js/angular-sanitize.min.js"></script>
    <script src="js/app.js"></script>
    <script src="js/common.js"></script>
  </body>
</html>
`
