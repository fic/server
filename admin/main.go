package main

import (
	"flag"
	"io/fs"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strconv"
	"strings"
	"syscall"

	"srs.epita.fr/fic-server/admin/api"
	"srs.epita.fr/fic-server/admin/generation"
	"srs.epita.fr/fic-server/admin/pki"
	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
	"srs.epita.fr/fic-server/settings"
)

func main() {
	var err error
	bind := "127.0.0.1:8081"
	cloudDAVBase := ""
	cloudUsername := "fic"
	cloudPassword := ""
	localImporterDirectory := ""
	gitImporterRemote := ""
	gitImporterBranch := ""
	localImporterSymlink := false
	baseURL := "/"
	checkplugins := sync.CheckPluginList{}

	// Read paremeters from environment
	if v, exists := os.LookupEnv("FICOIDC_ISSUER"); exists {
		api.OidcIssuer = v
	} else if v, exists := os.LookupEnv("FICOIDC_ISSUER_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open FICOIDC_ISSUER_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		api.OidcIssuer = strings.TrimSpace(string(b))

		fd.Close()
	}
	if v, exists := os.LookupEnv("FICOIDC_SECRET"); exists {
		api.OidcSecret = v
	} else if v, exists := os.LookupEnv("FICOIDC_SECRET_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open FICOIDC_SECRET_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		api.OidcSecret = strings.TrimSpace(string(b))

		fd.Close()
	}
	if v, exists := os.LookupEnv("FICCA_PASS"); exists {
		pki.SetCAPassword(v)
	} else if v, exists := os.LookupEnv("FICCA_PASS_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open FICCA_PASS_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		pki.SetCAPassword(strings.TrimSpace(string(b)))

		fd.Close()
	} else {
		log.Println("WARNING: no password defined for the CA, will use empty password to secure CA private key")
		log.Println("WARNING:    PLEASE DEFINE ENVIRONMENT VARIABLE: FICCA_PASS")
	}
	if v, exists := os.LookupEnv("FICCLOUD_URL"); exists {
		cloudDAVBase = v
	}
	if v, exists := os.LookupEnv("FICCLOUD_USER"); exists {
		cloudUsername = v
	}
	if v, exists := os.LookupEnv("FICCLOUD_PASS"); exists {
		cloudPassword = v
	} else if v, exists := os.LookupEnv("FICCLOUD_PASS_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open FICCLOUD_PASS_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		cloudPassword = strings.TrimSpace(string(b))

		fd.Close()
	}
	if v, exists := os.LookupEnv("FIC_BASEURL"); exists {
		baseURL = v
	}
	if v, exists := os.LookupEnv("FIC_4REAL"); exists {
		api.IsProductionEnv, err = strconv.ParseBool(v)
		if err != nil {
			log.Fatal("Unable to parse FIC_4REAL variable:", err)
		}
	}
	if v, exists := os.LookupEnv("FIC_ADMIN_BIND"); exists {
		bind = v
	}
	if v, exists := os.LookupEnv("FIC_TIMESTAMPCHECK"); exists {
		api.TimestampCheck = v
	}
	if v, exists := os.LookupEnv("FIC_SETTINGS"); exists {
		settings.SettingsDir = v
	}
	if v, exists := os.LookupEnv("FIC_FILES"); exists {
		fic.FilesDir = v
	}
	if v, exists := os.LookupEnv("FIC_SYNC_LOCALIMPORT"); exists {
		localImporterDirectory = v
	}
	if v, exists := os.LookupEnv("FIC_SYNC_LOCALIMPORTSYMLINK"); exists {
		localImporterSymlink, err = strconv.ParseBool(v)
		if err != nil {
			log.Fatal("Unable to parse FIC_SYNC_LOCALIMPORTSYMLINK variable:", err)
		}
	}
	if v, exists := os.LookupEnv("FIC_SYNC_GIT_IMPORT_REMOTE"); exists {
		gitImporterRemote = v
	}
	if v, exists := os.LookupEnv("FIC_SYNC_GIT_BRANCH"); exists {
		gitImporterBranch = v
	}
	if v, exists := os.LookupEnv("FIC_OPTIONALDIGEST"); exists {
		fic.OptionalDigest, err = strconv.ParseBool(v)
		if err != nil {
			log.Fatal("Unable to parse FIC_OPTIONALDIGEST variable:", err)
		}
	}
	if v, exists := os.LookupEnv("FIC_STRONGDIGEST"); exists {
		fic.StrongDigest, err = strconv.ParseBool(v)
		if err != nil {
			log.Fatal("Unable to parse FIC_STRONGDIGEST variable:", err)
		}
	}

	// Read parameters from command line
	flag.StringVar(&bind, "bind", bind, "Bind port/socket")
	var dsn = flag.String("dsn", fic.DSNGenerator(), "DSN to connect to the MySQL server")
	flag.StringVar(&baseURL, "baseurl", baseURL, "URL prepended to each URL")
	flag.StringVar(&api.TimestampCheck, "timestampCheck", api.TimestampCheck, "Path regularly touched by frontend to check time synchronisation")
	flag.StringVar(&pki.PKIDir, "pki", "./PKI", "Base directory where found PKI scripts")
	var staticDir = flag.String("static", "", "Directory containing static files (default if not provided: use embedded files)")
	flag.StringVar(&api.TeamsDir, "teams", "./TEAMS", "Base directory where save teams JSON files")
	flag.StringVar(&api.DashboardDir, "dashbord", "./DASHBOARD", "Base directory where save public JSON files")
	flag.StringVar(&settings.SettingsDir, "settings", settings.SettingsDir, "Base directory where load and save settings")
	flag.StringVar(&fic.FilesDir, "files", fic.FilesDir, "Base directory where found challenges files, local part")
	flag.StringVar(&generation.GeneratorSocket, "generator", "./GENERATOR/generator.socket", "Path to the generator socket (used to trigger issues.json generations, use an empty string to generate locally)")
	flag.StringVar(&localImporterDirectory, "localimport", localImporterDirectory,
		"Base directory where found challenges files to import, local part")
	flag.BoolVar(&localImporterSymlink, "localimportsymlink", localImporterSymlink,
		"Copy files or just create symlink?")
	flag.StringVar(&gitImporterRemote, "git-import-remote", gitImporterRemote,
		"Remote URL of the git repository to use as synchronization source")
	flag.StringVar(&gitImporterBranch, "git-branch", gitImporterBranch,
		"Branch to use in the git repository")
	flag.StringVar(&cloudDAVBase, "clouddav", cloudDAVBase,
		"Base directory where found challenges files to import, cloud part")
	flag.StringVar(&cloudUsername, "clouduser", cloudUsername, "Username used to sync")
	flag.StringVar(&cloudPassword, "cloudpass", cloudPassword, "Password used to sync")
	flag.BoolVar(&fic.OptionalDigest, "optionaldigest", fic.OptionalDigest, "Is the digest required when importing files?")
	flag.BoolVar(&fic.StrongDigest, "strongdigest", fic.StrongDigest, "Are BLAKE2b digests required or is SHA-1 good enough?")
	flag.BoolVar(&api.IsProductionEnv, "4real", api.IsProductionEnv, "Set this flag when running for a real challenge (it disallows or avoid most of mass user progression deletion)")
	flag.Var(&checkplugins, "rules-plugins", "List of libraries containing others rules to checks")
	flag.Var(&sync.RemoteFileDomainWhitelist, "remote-file-domain-whitelist", "List of domains which are allowed to store remote files")
	flag.Parse()

	log.SetPrefix("[admin] ")

	// Instantiate importer
	if localImporterDirectory != "" && cloudDAVBase != "" {
		log.Fatal("Cannot have both --clouddav and --localimport defined.")
		return
	} else if gitImporterRemote != "" && cloudDAVBase != "" {
		log.Fatal("Cannot have both --clouddav and --git-import-remote defined.")
		return
	} else if gitImporterRemote != "" {
		sync.GlobalImporter = sync.NewGitImporter(sync.LocalImporter{Base: localImporterDirectory, Symlink: localImporterSymlink}, gitImporterRemote, gitImporterBranch)
	} else if localImporterDirectory != "" {
		sync.GlobalImporter = sync.LocalImporter{Base: localImporterDirectory, Symlink: localImporterSymlink}
	} else if cloudDAVBase != "" {
		sync.GlobalImporter, _ = sync.NewCloudImporter(cloudDAVBase, cloudUsername, cloudPassword)
	}
	if sync.GlobalImporter != nil {
		if err := sync.GlobalImporter.Init(); err != nil {
			log.Fatal("Unable to initialize the importer: ", err.Error())
		}
		log.Println("Using", sync.GlobalImporter.Kind())

		// Update distributed challenge.json
		if _, err := os.Stat(path.Join(settings.SettingsDir, settings.ChallengeFile)); os.IsNotExist(err) {
			challengeinfo, err := sync.GetFileContent(sync.GlobalImporter, settings.ChallengeFile)
			if err == nil {
				if fd, err := os.Create(path.Join(settings.SettingsDir, settings.ChallengeFile)); err != nil {
					log.Fatal("Unable to open SETTINGS/challenge.json:", err)
				} else {
					fd.Write([]byte(challengeinfo))
					err = fd.Close()
					if err != nil {
						log.Fatal("Something went wrong during SETTINGS/challenge.json writing:", err)
					}
				}
			}
		}
	}

	// Sanitize options
	log.Println("Checking paths...")
	if staticDir != nil && *staticDir != "" {
		if sDir, err := filepath.Abs(*staticDir); err != nil {
			log.Fatal(err)
		} else {
			log.Println("Serving pages from", sDir)
			staticFS = http.Dir(sDir)
			sync.DeepReportPath = path.Join(sDir, sync.DeepReportPath)
		}
	} else {
		sub, err := fs.Sub(assets, "static")
		if err != nil {
			log.Fatal("Unable to cd to static/ directory:", err)
		}
		log.Println("Serving pages from memory.")
		staticFS = http.FS(sub)

		sync.DeepReportPath = path.Join("SYNC", sync.DeepReportPath)
		if _, err := os.Stat("SYNC"); os.IsNotExist(err) {
			os.MkdirAll("SYNC", 0751)
		}
	}
	if fic.FilesDir, err = filepath.Abs(fic.FilesDir); err != nil {
		log.Fatal(err)
	}
	if pki.PKIDir, err = filepath.Abs(pki.PKIDir); err != nil {
		log.Fatal(err)
	}
	if api.DashboardDir, err = filepath.Abs(api.DashboardDir); err != nil {
		log.Fatal(err)
	}
	if api.TeamsDir, err = filepath.Abs(api.TeamsDir); err != nil {
		log.Fatal(err)
	}
	if api.TimestampCheck, err = filepath.Abs(api.TimestampCheck); err != nil {
		log.Fatal(err)
	}
	if settings.SettingsDir, err = filepath.Abs(settings.SettingsDir); err != nil {
		log.Fatal(err)
	}
	if baseURL != "/" {
		baseURL = path.Clean(baseURL)
	} else {
		baseURL = ""
	}

	// Creating minimal directories structure
	os.MkdirAll(fic.FilesDir, 0751)
	os.MkdirAll(pki.PKIDir, 0711)
	os.MkdirAll(api.TeamsDir, 0751)
	os.MkdirAll(api.DashboardDir, 0751)
	os.MkdirAll(settings.SettingsDir, 0751)

	// Load rules plugins
	for _, p := range checkplugins {
		if err := sync.LoadChecksPlugin(p); err != nil {
			log.Fatalf("Unable to load rule plugin %q: %s", p, err.Error())
		} else {
			log.Printf("Rules plugin %q successfully loaded", p)
		}
	}

	// Initialize settings and load them
	if !settings.ExistsSettings(path.Join(settings.SettingsDir, settings.SettingsFile)) {
		if err = api.ResetSettings(); err != nil {
			log.Fatal("Unable to initialize settings.json:", err)
		}
	}
	var config *settings.Settings
	if config, err = settings.ReadSettings(path.Join(settings.SettingsDir, settings.SettingsFile)); err != nil {
		log.Fatal("Unable to read settings.json:", err)
	} else {
		api.ApplySettings(config)
	}

	// Initialize dashboard presets
	if err = api.InitDashboardPresets(api.DashboardDir); err != nil {
		log.Println("Unable to initialize dashboards presets:", err)
	}

	// Database connection
	log.Println("Opening database...")
	if err = fic.DBInit(*dsn); err != nil {
		log.Fatal("Cannot open the database: ", err)
	}
	defer fic.DBClose()

	log.Println("Creating database...")
	if err = fic.DBCreate(); err != nil {
		log.Fatal("Cannot create database: ", err)
	}

	// Update base URL on main page
	log.Println("Changing base URL to", baseURL+"/", "...")
	genIndex(baseURL)

	// Prepare graceful shutdown
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	app := NewApp(config, baseURL, bind)
	go app.Start()

	// Wait shutdown signal
	<-interrupt

	log.Print("The service is shutting down...")
	app.Stop()
	log.Println("done")
}
