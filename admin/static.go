package main

import (
	"bytes"
	"embed"
	"errors"
	"log"
	"net/http"
	"path"
	"strings"
	"text/template"

	"srs.epita.fr/fic-server/admin/api"
	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
	"srs.epita.fr/fic-server/settings"

	"github.com/gin-gonic/gin"
)

//go:embed static

var assets embed.FS

var indexPage []byte

func genIndex(baseURL string) {
	b := bytes.NewBufferString("")
	if indexTmpl, err := template.New("index").Parse(indextpl); err != nil {
		log.Fatal("Cannot create template:", err)
	} else if err = indexTmpl.Execute(b, map[string]string{"urlbase": path.Clean(path.Join(baseURL+"/", "nuke"))[:len(path.Clean(path.Join(baseURL+"/", "nuke")))-4]}); err != nil {
		log.Fatal("An error occurs during template execution:", err)
	} else {
		indexPage = b.Bytes()
	}
}

func serveIndex(c *gin.Context) {
	c.Writer.Write(indexPage)
}

var staticFS http.FileSystem

func serveFile(c *gin.Context, url string) {
	c.Request.URL.Path = url
	http.FileServer(staticFS).ServeHTTP(c.Writer, c.Request)
}

func declareStaticRoutes(router *gin.RouterGroup, cfg *settings.Settings, baseURL string) {
	router.GET("/", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/claims/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/exercices/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/events/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/files", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/public/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/pki/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/repositories", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/settings", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/sync", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/tags/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/teams/*_", func(c *gin.Context) {
		serveIndex(c)
	})
	router.GET("/themes/*_", func(c *gin.Context) {
		serveIndex(c)
	})

	router.GET("/css/*_", func(c *gin.Context) {
		serveFile(c, strings.TrimPrefix(c.Request.URL.Path, baseURL))
	})
	router.GET("/fonts/*_", func(c *gin.Context) {
		serveFile(c, strings.TrimPrefix(c.Request.URL.Path, baseURL))
	})
	router.GET("/img/*_", func(c *gin.Context) {
		serveFile(c, strings.TrimPrefix(c.Request.URL.Path, baseURL))
	})
	router.GET("/js/*_", func(c *gin.Context) {
		serveFile(c, strings.TrimPrefix(c.Request.URL.Path, baseURL))
	})
	router.GET("/views/*_", func(c *gin.Context) {
		serveFile(c, strings.TrimPrefix(c.Request.URL.Path, baseURL))
	})

	router.GET("/files/*_", func(c *gin.Context) {
		// TODO: handle .gz file here
		http.ServeFile(c.Writer, c.Request, path.Join(fic.FilesDir, strings.TrimPrefix(c.Request.URL.Path, path.Join(baseURL, "files"))))
	})
	router.GET("/submissions/*_", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, path.Join(api.TimestampCheck, strings.TrimPrefix(c.Request.URL.Path, path.Join(baseURL, "submissions"))))
	})
	router.GET("/vids/*_", func(c *gin.Context) {
		if importer, ok := sync.GlobalImporter.(sync.DirectAccessImporter); ok {
			http.ServeFile(c.Writer, c.Request, importer.GetLocalPath(strings.TrimPrefix(c.Request.URL.Path, path.Join(baseURL, "vids"))))
		} else {
			c.AbortWithError(http.StatusBadRequest, errors.New("Only available with local importer."))
		}
	})

	router.GET("/check_import.html", func(c *gin.Context) {
		serveFile(c, "check_import.html")
	})
	router.GET("/full_import_report.json", func(c *gin.Context) {
		http.ServeFile(c.Writer, c.Request, sync.DeepReportPath)
	})
}
