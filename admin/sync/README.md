Détails de l'aborescence attendue
---------------------------------

Tous les textes doivent utiliser l'encodage UTF8.

- Un dossier par thème `IDTEAM-Nom du thème` (`IDTEAM` peut être un mot, sans tiret `-` ; le nom du thème est celui qui sera affiché dans l'interface, soyez créatifs !), contenant :
    * `AUTHORS.txt` avec vos noms, tels qu'ils apparraîtront sur le site (voir exemple [plus bas](#exemple-authorstxt))
    * `overview.txt` une présentation rapide du scenario (~2-3 phrases d'accroche pour lecture rapide), compréhensible par un décideur, petit schéma à l'appui
    * `heading.jpg` une photographie libre de tout droit (également celui de référence) représentant le thème
    * Un dossier par challenge : `CHID-Titre du challenge` (avec `CHID` l'identifiant numérique du challenge, typiquement son numéro d'ordre), contenant :
        + `overview.txt` une présentation rapide du challenge (~1-2 phrases d'accroche), compréhensible par un décideur, petit schéma à l'appui si besoin
        + `statement.txt` contenant le scénario du challenge, tel qu'il sera affiché sur le site, à destination des participants
        + `finished.txt` (facultatif) contenant un texte affiché au participant ayant validé l'exercice : par exemple pour donner plus d'informations sur les vulnérabilités rencontrées
        + `challenge.txt` définitions des paramètres de votre challenge (au format [toml](https://github.com/toml-lang/toml/blob/master/versions/en/toml-v0.4.0.md)) :
            - `gain = 42` : nombre de points que rapporte cet exercice ;
            - `tags = ["Android", "RAT", "ROM"]` : mots-clefs de l'exercice ;
            - `[[depend]]` : dépendance à un autre exercice :
                * `id = CHID` : identifiant du challenge ;
                * `theme = "NomDuTheme"` : (facultatif) nom du thème dans lequel aller chercher l'identifiant (par défaut, on prend le thème courant) ;
            - `[[flag]]` : drapeau classique à valider pour résoudre le challenge :
			    * `id = 42` : (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;
                * `label = "Intitulé"` : (facultatif, par défaut : `Flag`) intitulé du drapeau ;
                * `raw = 'MieH2athxuPhai6u'` ou `raw = ['part1', 'part2']` : drapeau exact à trouver ; sous forme de tableau, le participant n'aura pas connaissaance du nombre d'éléments ;
		    	* `validator_regexp = "^(?:sudo +)?(.*)$"` : (facultatif) expression rationnelle dont les groupes capturés serviront comme chaîne à valider (notez que `?:` au début d'un groupe ne le capturera pas) ;
                * `ordered = false` : (facultatif, par défaut : `false`) ignore l'ordre dans lequels les éléments du tableau sont passés ;
                * `ignorecase = true` : (facultatif, par défaut : `false`) ignore la case de ce drapeau ;
		    	* `placeholder = "Indication"` : (facultatif) chaîne de caractères placée sous le champ du formulaire, idéale pour donner une indication de format ;
				* `[[flag.unlock_file]]` : bloque l'accès à un fichier tant que le flag n'est pas obtenu :
					+ `filename = "toto.txt"` : nom du fichier tel qu'il apparaît dans le dossier `files` ;
				* `[[flag.need_flag]]` : liste des flags devant être validés avant de débloquer celui-ci :
					+ `id = 23` : identifiant du flag tel qu'il a été défini plus tôt dans le fichier ;
            - `[[flag_mcq]]` : drapeau sous forme de question à choix multiple (cases à cocher) :
                * `label = "Intitulé du groupe"` : (facultatif) intitulé du groupe de choix ;
                * `[[flag_mcq.choice]]` : représente un choix, répétez autant de fois qu'il y a de choix :
		            + `label = "Intitulé de la réponse"`,
                    + `value = true` : (facultatif, par défaut `false`) valeur attendue pour ce choix ; pour un QCM justifié, utilisez une chaîne de caractères (notez qu'il n'est pas possible de combiner des réponses vraies justifiées et justifiées),
					+ `placeholder = "Flag correspondant"` : (facultatif) indication affichée dans le champ de texte des QCM justifiés ;
            - `[[flag_ucq]]` : drapeau sous forme de question à choix unique :
			    * `id = 42` : (facultatif) identifiant du flag au sein de l'exercice, pour définir des dépendances ;
                * `label = "Intitulé du groupe"` : (facultatif) intitulé du groupe de choix ;
                * `raw = 'MieH2athxuPhai6u'` : drapeau attendu parmi les propositions ;
		    	* `validator_regexp = "^(?:sudo +)?(.*)$"` : (facultatif) expression rationnelle dont les groupes capturés serviront comme chaîne à valider (notez que `?:` au début d'un groupe ne le capturera pas) ;
    			* `placeholder = "Indication"` : (facultatif, uniquement si `displayAs = select`) chaîne de caractères placée sous le champ du formulaire ;
                * `displayAs = "select|radio"` : (facultatif, par défaut `radio`) manière dont est affichée le choix : `select` pour une liste de choix, `radio` pour des boutons radios ;
                * `choices_cost = 20` : (facultatif, par défaut `0`) coût pour afficher les choix, avant l'affichage, se comporte comme un `flag` classique (à 0, les choix sont affichés directement) ;
                * `[[flag_ucq.choice]]` : représente un choix, répétez autant de fois qu'il y a de choix :
    			    + `value = "response"` : valeur qui sera retournée pour comparaison avec la valeur `raw` du ucq,
	    		    + `label = "Intitulé de la réponse"` : (facultatif, par défaut identique à `value`) ;
				* `[[flag_ucq.unlock_file]]` : bloque l'accès à un fichier tant que le flag n'est pas obtenu :
					+ `filename = "toto.txt"` : nom du fichier tel qu'il apparaît dans le dossier `files` ;
				* `[[flag_ucq.need_flag]]` : liste des flags devant être validés avant de débloquer celui-ci :
					+ `id = 23` : identifiant du flag tel qu'il a été défini plus tôt dans le fichier ;
            - `[[hint]]` : paramètres pour un indice :
                * `filename = "toto.txt"` : (mutuellement exclusif avec `content`) nom du fichier tel qu'il apparaît dans le dossier `hints` ;
                * `content = "Contenu de l'indice"` : (mutuellement exclusif avec `filename`) contenu de l'indice affiché, en markdown ;
                * `cost = 10` : (facultatif, par défaut 1/4 des gains du challenge) coût de l'indice ;
                * `title = "Foo Bar"` : (facultatif, par défaut "Astuce $id") titre de l'astuce dans l'interface ;
        + `links.txt` : webographie publiée avec les solutions
            - un lien par ligne
            - format d'une ligne : `https://lien Description`
              le premier ' ' est utilisé comme séparateur entre le lien et sa description
            - liens vers les CVE concernées, metasploit/exploitDB, article qui vous a aidé, extrait/dépôt de code, ...
        + `hints/` : dossier contenant des indices pour orienter le participant (qu'il débloquera en échange d'un certain nombre de points)
            - `DIGESTS.txt` : contenant les condensats des fichiers : `$(b2sum * > DIGESTS.txt)` à générer avant l'upload !
            - les fichiers textes de moins de 25 lignes sont affichés directement, les autres (autres types ou textes plus longs) sont proposés au téléchargement.
        + `files/` : fichiers à distribuer aux participants
            - `DIGESTS.txt` : contenant les condensats des fichiers : `$(b2sum * > DIGESTS.txt)` à générer avant l'upload !
            - Pas plus  4GB à télécharger **par challenge** (ie. tous les fichiers de ce challenge)
            - Archives `.tar.bz2`, `.tar.gz`, `.tar.xz` ou `.zip` lorsque nécessaire. **PAS** de `.rar`, ...
            - Compresser (sans tarball, juste via **gzip**) les fichiers lorsque c'est utile (memory dump, images BMP, disques, fichiers textes, ...)
            - Utiliser `$(split -b 240M -d BIG_FILE BIG_FILE.)` pour uploader les gros fichiers sur owncloud.
              Ces fichiers seront concaténés au moment de leur import sur l'interface.
              Seul le hash du fichier entier est requis dans le fichier `DIGESTS.txt`.
        + `resolution.mp4` : la vidéo de résolution, montée :
            - format MP4 (H.264 + AAC + 3GPP Timed Text)
            - utiliser les sous-titres pour commenter les étapes ; pas de commentaires audio
            - environ 2' par vidéo : maxi 1'30" pour les challenges simples, 3-4' maxi
            - `ffmpeg -video_size 1920x1080 -framerate 25 -f x11grab -i :0.0 -f alsa -ac 2 -i hw:0 -strict experimental resolution.mp4`
            - [recordMyDesktop](http://recordmydesktop.sourceforge.net/) sous Linux
            - [Screencast Capture Lite](http://cesarsouza.github.io/screencast-capture/) pour Windows (pas de logiciel de « démonstration » => il faut payer la licence pour publier une vidéo)
            - [Aegisub](http://www.aegisub.org/), [Gnome Subtitle](http://gnomesubtitles.org/), emacs/vim, ... pour les sous-titres
        + `ressources/` :
            - ressources et scripts que vous avez réalisés pour le challenge : pour sa construction ou sa résolution
            - schéma du SI au premier challenge concerné
            - éventuellement un `README.txt` avec les liens des outils externes


Exemple d'arborescence
----------------------

    SATURN-Active Directory/
        ...
    SATURN-Virtualisation légère/
        ...
    JUPITER-PDF/
        AUTHORS.txt
        overview.txt
        1-Cible cachée/
            ...
        2-Black&White/
            ...
        3-Ligne rouge/
            files/
                DIGESTS.txt
                clue.pdf
                big_clue.pdf.00
                big_clue.pdf.01
                big_clue.pdf.02
            ressources/
                generator.pl
                PDFextractor.exe
                solver.py
                pdf_index_schema.svg
            challenge.txt
            links.txt
            overview.txt
            resolution.mp4
            scenario.txt
    ...


Exemple `AUTHORS.txt`  {#exemple-authorstxt}
---------------------

```
Courtois J. <mailto:courto_j@epita.fr>
Bombal S.
Mercier P-O. <https://nemunai.re/>
```

Vous pouvez indiquer entre chevrons, un lien qui sera associé à votre nom.

Vous pouvez utiliser un pseudo si vous n'êtes pas fier de vos réalisations.


Exemple `challenge.txt`  {#exemple-challengetxt}
-----------------------

```
gain = 42

[[depend]]
id = 2

[[flag]]
label = "Date d'exfiltration"
placeholder = "Format : yyyy-mm"
raw = '2015-12'

[[flag]]
label = "IPv6 d'exfiltration"
raw = 'fe80::319c:1002:7c60:68fa'
ignorecase = true

[[flag_ucq]]
label = "Conditions générales de validation de challenge"
raw = 'conscent'

  [[flag_ucq.choice]]
  label = "J'accepte les conditions"
  value = 'conscent'

[[flag_ucq]]
label = "Quelle est la couleur du cheval blanc d'Henri IV ?"
raw = 'blanc'
ignorecase = true
displayAs = "select"

  [[flag_ucq.choice]]
  value = 'Noir'

  [[flag_ucq.choice]]
  label = 'Roux'
  value = 'Alezan'

  [[flag_ucq.choice]]
  label = 'Brun'
  value = 'Alezan'

  [[flag_ucq.choice]]
  label = "Crème"
  value = 'Blanc'

[[flag_mcq]]
label = "Quels sont les films réalisés par C. Nolan ?"

  [[flag_mcq.choice]]
  label = "Memento"
  value = true

  [[flag_mcq.choice]]
  label = "Inception"
  value = true

  [[flag_mcq.choice]]
  label = "Transcendance"

[[hint]]
filename = 'enocean-specs.pdf'
title = "Spécifications du protocole utilisé"

[[hint]]
content = """
Le TOML c'est magique.
Je peux avoir des chaînes de caractères sur plusieurs lignes !
"""
title = "L'astuce du siècle"
cost = 30
```


Exemple `links.txt`  {#exemple-linkstxt}
-------------------

```
https://media.ccc.de/... Vidéo d'inspiration
https://metasplo.it/ Exploit utilisé
https://nist.gov/ CVE-2016-4242
```


Exemple `DIGESTS.txt`  {#exemple-digeststxt}
---------------------

```
3222734c6c8782682a9c36135a3518e8f4d1facabf76e702cf50da0037a4ed0a425e51266c2914fb83828573e397f96c2a95d419bd85919055479d028f51dba5  fic2016.jpg
023939b0c52b0dfce66954318ab82f7a8c10af4c79c8d5781612b58c74f3ace056067d7b15967e612b176a186b46d3d900c4db8881ba47202521eec33e5bb87b  fic.org
7c91450239cf9b0717642c55c3429dd7326db26e87d4ca198758053333f0640ee89d2dd9b2f1919598f89644b06aa8fc2085648e3d1e542a6db324c9b16a0bdf  header.tex
```

---
title: Format des répertoires pour la synchronisation
author: FIC team 2019
date: "Dernière mise à jour du document : 1 décembre 2018"
---
