package sync

import (
	"path"
	"strings"

	"srs.epita.fr/fic-server/libfic"
	"srs.epita.fr/fic-server/settings"
)

// ImportChallengeInfo imports images defined in the challengeinfo.
func ImportChallengeInfo(ci *settings.ChallengeInfo, dashboardDir string) (err error) {
	if len(ci.MainLogo) > 0 {
		for i, logo := range ci.MainLogo {
			dest := path.Join(fic.FilesDir, "logo", path.Base(logo))
			err = importFile(GlobalImporter, logo, dest)
			if err != nil {
				return
			}

			ci.MainLogo[i] = path.Join("$FILES$", strings.TrimPrefix(dest, fic.FilesDir))
		}
	}

	if len(ci.DashboardBackground) > 0 {
		dest := path.Join(dashboardDir, path.Base(ci.DashboardBackground))
		err = importFile(GlobalImporter, ci.DashboardBackground, dest)
		if err != nil {
			return
		}
	}

	if len(ci.Partners) > 0 {
		for i, partner := range ci.Partners {
			dest := path.Join(fic.FilesDir, "partner", path.Base(partner.Src))
			err = importFile(GlobalImporter, partner.Src, dest)
			if err != nil {
				return
			}

			ci.Partners[i].Src = path.Join("$FILES$", strings.TrimPrefix(dest, fic.FilesDir))
		}
	}

	return nil
}
