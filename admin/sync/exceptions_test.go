package sync

import (
	"testing"
)

const sampleFile = `0-exercice-1/overview.md:spelling:Sterik
0-exercice-1/overview.md:spelling:RSSI
0-exercice-1/statement.md:spelling:Sterik
0-exercice-1/statement.md:spelling:RSSI
0-exercice-1/finished.md:spelling:GoPhish
0-exercice-1/resolution.md:not-forbidden-string:51.38.152.16
0-exercice-1/resolution.md:not-forbidden-string:109.57.42.65
0-exercice-1/resolution.md:not-forbidden-string:server_update.sh
0-exercice-1/resolution.md:spelling:Flag
0-exercice-1/resolution.md:spelling:MHA
0-exercice-1/resolution.md:spelling:hops
0-exercice-1/resolution.md:4:g3__gn_les_2m__b1_a4_1
0-exercice-1/resolution.md:10:g3__gn_les_2m__b1_a4_1
0-exercice-1/resolution.md:spelling:echo
0-exercice-1/resolution.md:spelling:nbash
0-exercice-1/resolution.md:spelling:bash
0-exercice-1/resolution.md:spelling:update
0-exercice-1/resolution.md:spelling:sh
0-exercice-1/resolution.md:spelling:Success
0-exercice-1/resolution.md:11:typo_guillemets_typographiques_doubles_fermants
0-exercice-1/resolution.md:spelling:cronjob
0-exercice-1/resolution.md:spelling:Level
challenge.toml:spelling:time
challenge.toml:spelling:ago
0-exercice-1/resolution.md:spelling:SCL
challenge.toml:spelling:SCL`

func TestLoadExceptions(t *testing.T) {
	exceptions := ParseExceptionString(sampleFile, nil)

	if len(*exceptions) != 26 {
		t.Fatalf("Expected 26 exceptions, got %d", len(*exceptions))
	}
}

func TestFilterExceptions(t *testing.T) {
	exceptions := ParseExceptionString(sampleFile, nil)

	filteredExceptions := exceptions.GetFileExceptions("resolution.md")
	if len(*filteredExceptions) != 1 {
		t.Fatalf("Expected 1 exceptions, got %d", len(*filteredExceptions))
	}

	filteredExceptions = exceptions.GetFileExceptions("challenge.toml")
	if len(*filteredExceptions) != 3 {
		t.Fatalf("Expected 3 exceptions, got %d", len(*filteredExceptions))
	}

	filteredExceptions = exceptions.GetFileExceptions("0-exercice-1")
	if len(*filteredExceptions) != 23 {
		t.Fatalf("Expected 23 exceptions, got %d", len(*filteredExceptions))
	}

	filteredExceptions = exceptions.Filter2ndCol("spelling")
	if len(*filteredExceptions) != 20 {
		t.Fatalf("Expected 20 exceptions, got %d", len(*filteredExceptions))
	}
}
