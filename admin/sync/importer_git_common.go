package sync

import (
	"io"
	"net/url"
	"os"
	"regexp"
	"sync"
)

var gitRemoteRe = regexp.MustCompile(`^(?:(?:git@|https://)([\w.@]+)(?:/|:))((?:[\w-_]+)/(?:[\w-_/]+))(?:.git){0,1}(?:(?:/){0,1})$`)

var oneGitPull sync.Mutex

func OneGitPullStatus() bool {
	if oneGitPull.TryLock() {
		oneGitPull.Unlock()
		return true
	}
	return false
}

func countFileInDir(dirname string) (int, error) {
	files, err := os.ReadDir(dirname)
	if err != nil {
		return 0, err
	}

	return len(files), nil
}

func (i GitImporter) Exists(filename string) bool {
	return i.li.Exists(filename)
}

func (i GitImporter) toURL(filename string) string {
	return i.li.toURL(filename)
}

func (i GitImporter) GetLocalPath(filename ...string) string {
	return i.li.GetLocalPath(filename...)
}

func (i GitImporter) importFile(URI string, next func(string, string) (interface{}, error)) (interface{}, error) {
	return i.li.importFile(URI, next)
}

func (i GitImporter) GetFile(filename string) (io.Reader, error) {
	return i.li.GetFile(filename)
}

func (i GitImporter) writeFile(filename string, reader io.Reader) error {
	return i.li.writeFile(filename, reader)
}

func (i GitImporter) ListDir(filename string) ([]string, error) {
	return i.li.ListDir(filename)
}

func (i GitImporter) Stat(filename string) (os.FileInfo, error) {
	return i.li.Stat(filename)
}

func (i GitImporter) Kind() string {
	return "git originated from " + i.Remote + " on " + i.li.Kind()
}

func (i GitImporter) DeleteDir(filename string) error {
	return i.li.DeleteDir(filename)
}

func getForgeBaseLink(remote string) (u *url.URL, err error) {
	res := gitRemoteRe.FindStringSubmatch(remote)
	u, err = url.Parse(res[2])
	u.Scheme = "https"
	u.Host = res[1]
	return
}

type GitSubmoduleStatus struct {
	Hash   string `json:"hash"`
	Text   string `json:"text,omitempty"`
	Path   string `json:"path"`
	Branch string `json:"branch"`
}
