package main

import (
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"os"

	"srs.epita.fr/fic-server/libfic"
)

type wantChoices struct {
	FlagId int `json:"id"`
}

func treatWantChoices(pathname string, team *fic.Team) {
	// Generate a unique identifier to follow the request in logs
	bid := make([]byte, 5)
	binary.LittleEndian.PutUint32(bid, rand.Uint32())
	id := "[" + base64.StdEncoding.EncodeToString(bid) + "]"
	log.Println(id, "New wantChoices receive", pathname)

	var ask wantChoices

	cnt_raw, err := ioutil.ReadFile(pathname)
	if err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
		return
	}

	err = json.Unmarshal(cnt_raw, &ask)
	if err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
		return
	}

	if ask.FlagId == 0 {
		log.Printf("%s [WRN] Invalid content in wantChoices file:  %s\n", id, pathname)
		os.Remove(pathname)
		return
	}

	flag, err := fic.GetFlagKey(ask.FlagId)
	if err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
		return
	}

	if !team.CanSeeFlag(flag) {
		log.Printf("%s [!!!] The team asks to display choices whereas it doesn't have access to the flag\n", id)
		return
	}

	exercice, err := flag.GetExercice()
	if err != nil {
		log.Printf("%s [ERR] Unable to retrieve the flag's underlying exercice: %s\n", id, err)
		return
	}

	if !team.HasAccess(exercice) {
		log.Printf("%s [!!!] The team asks to display choices whereas it doesn't have access to the exercice\n", id)
		return
	}

	if exercice.Disabled {
		log.Println("[!!!] The team submits something for a disabled exercice")
		return
	}

	if exercice.IdTheme != nil {
		theme, err := fic.GetTheme(*exercice.IdTheme)
		if err != nil {
			log.Printf("%s [ERR] Unable to retrieve theme for exercice %d: %s\n", id, exercice.Id, err)
			return
		}

		// Theme should not be locked
		if theme.Locked {
			log.Printf("%s [!!!] Want choice received for locked theme %d (fid=%d): %s\n", id, exercice.IdTheme, ask.FlagId, theme.Name)
			return
		}
	}

	err = team.DisplayChoices(flag)
	if err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
		return
	}

	appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
	if err = os.Remove(pathname); err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
	}
}
