package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"strings"
	"time"

	"srs.epita.fr/fic-server/libfic"
)

var generatorSocket string

func appendGenQueue(gs fic.GenStruct) error {
	buf, err := json.Marshal(gs)
	if err != nil {
		return fmt.Errorf("Something is wrong with JSON encoder: %w", err)
	}

	sockType := "unix"
	if strings.Contains(generatorSocket, ":") {
		sockType = "tcp"
	}

	socket, err := net.Dial(sockType, generatorSocket)
	if err != nil {
		log.Printf("Unable to contact generator at: %s, retring in 1 second", generatorSocket)
		time.Sleep(time.Second)
		return appendGenQueue(gs)
	}
	defer socket.Close()

	httpClient := &http.Client{
		Transport: &http.Transport{
			Dial: func(network, addr string) (net.Conn, error) {
				return socket, nil
			},
		},
	}

	resp, err := httpClient.Post("http://localhost/enqueue", "application/json", bytes.NewReader(buf))
	if err != nil {
		return fmt.Errorf("Unable to enqueue new generation event: %w", err)
	}
	resp.Body.Close()

	return nil
}
