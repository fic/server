package main

import (
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"math/rand"
	"os"

	"srs.epita.fr/fic-server/libfic"
)

type askOpenHint struct {
	HintId int64 `json:"id"`
}

func treatOpeningHint(pathname string, team *fic.Team) {
	// Generate a unique identifier to follow the request in logs
	bid := make([]byte, 5)
	binary.LittleEndian.PutUint32(bid, rand.Uint32())
	id := "[" + base64.StdEncoding.EncodeToString(bid) + "]"
	log.Println(id, "New openingHint receive", pathname)

	var ask askOpenHint

	cnt_raw, err := ioutil.ReadFile(pathname)
	if err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
		return
	}

	err = json.Unmarshal(cnt_raw, &ask)
	if err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
		return
	}

	if ask.HintId == 0 {
		log.Printf("%s [WRN] Invalid content in hint file: %s\n", id, pathname)
		os.Remove(pathname)
		return
	}

	hint, err := fic.GetHint(ask.HintId)
	if err != nil {
		log.Printf("%s [ERR] Unable to retrieve the given hint: %s\n", id, err)
		return
	}

	exercice, err := hint.GetExercice()
	if err != nil {
		log.Printf("%s [ERR] Unable to retrieve the hint's underlying exercice: %s\n", id, err)
		return
	}

	if exercice.Disabled {
		log.Println("[!!!] The team submits something for a disabled exercice")
		return
	}

	if !team.HasAccess(exercice) {
		log.Printf("%s [!!!] The team asks to open an hint whereas it doesn't have access to the exercice\n", id)
		return
	}

	if !team.CanSeeHint(hint) {
		log.Printf("%s [!!!] The team asks to open an hint whereas it doesn't have access to it due to hint dependencies\n", id)
		return
	}

	// Find the corresponding theme
	var theme *fic.Theme
	if exercice.IdTheme != nil {
		theme, err = fic.GetTheme(*exercice.IdTheme)
		if err != nil {
			log.Printf("%s [ERR] Unable to retrieve theme for exercice %d: %s\n", id, exercice.Id, err)
			return
		}

		// Theme should not be locked
		if theme.Locked {
			log.Printf("%s [!!!] Open hint received for locked theme %d (hid=%d): %s\n", id, exercice.IdTheme, ask.HintId, theme.Name)
			return
		}
	}

	if err = team.OpenHint(hint); err != nil && !fic.DBIsDuplicateKeyError(err) { // Skip DUPLICATE KEY errors
		log.Printf("%s [ERR] Unable to open hint: %s\n", id, err)
		return
	}

	if theme == nil {
		if _, err = fic.NewEvent(fmt.Sprintf("L'équipe %s a dévoilé un indice pour le défi %s&#160;!", html.EscapeString(team.Name), exercice.Title), "info"); err != nil {
			log.Printf("%s [WRN] Unable to create event: %s\n", id, err)
		}
	} else {
		// Write event
		if lvl, err := exercice.GetLevel(); err != nil {
			log.Printf("%s [WRN] %s\n", id, err)
		} else if _, err = fic.NewEvent(fmt.Sprintf("L'équipe %s a dévoilé un indice pour le <strong>%d<sup>e</sup></strong> défi %s&#160;!", html.EscapeString(team.Name), lvl, theme.Name), "info"); err != nil {
			log.Printf("%s [WRN] Unable to create event: %s\n", id, err)
		}
	}

	appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
	appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenEvents})
	if err = os.Remove(pathname); err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
	}
}
