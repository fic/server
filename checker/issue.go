package main

import (
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"io/ioutil"
	"log"
	"math/rand"
	"os"

	"srs.epita.fr/fic-server/libfic"
)

type IssueUpload struct {
	Id          int64  `json:"id"`
	IdExercice  int64  `json:"id_exercice"`
	Subject     string `json:"subject"`
	Description string `json:"description"`
}

func treatIssue(pathname string, team *fic.Team) {
	// Generate a unique identifier to follow the request in logs
	bid := make([]byte, 5)
	binary.LittleEndian.PutUint32(bid, rand.Uint32())
	id := "[" + base64.StdEncoding.EncodeToString(bid) + "]"
	log.Println(id, "New issue receive", pathname)

	var issue IssueUpload

	if cnt_raw, err := ioutil.ReadFile(pathname); err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
	} else if err := json.Unmarshal(cnt_raw, &issue); err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
	} else if len(issue.Subject) == 0 && len(issue.Description) == 0 {
		if err = os.Remove(pathname); err != nil {
			log.Printf("%s [ERR] %s\n", id, err)
		}
		log.Printf("%s Empty issue: not treated.\n", id)
	} else if len(issue.Subject) == 0 {
		if issue.Id <= 0 {
			if err = os.Remove(pathname); err != nil {
				log.Printf("%s [ERR] %s\n", id, err)
			}
			log.Printf("%s Issue with no subject: not treated.\n", id)
		} else if claim, err := team.GetClaim(issue.Id); err != nil {
			log.Printf("%s [ERR] Team id=%d,name=%q tries to access issue id=%d, but not granted: %s.\n", id, team.Id, team.Name, issue.Id, err)
		} else if len(issue.Description) == 0 {
			if err = os.Remove(pathname); err != nil {
				log.Printf("%s [ERR] %s\n", id, err)
			}
			log.Printf("%s Empty issue: not treated.\n", id)
		} else if desc, err := claim.AddDescription(issue.Description, &fic.ClaimAssignee{Id: 0}, true); err != nil {
			log.Printf("%s [WRN] Unable to add description to issue: %s\n", id, err)
		} else {
			claim.State = "new"
			claim.Update()

			log.Printf("%s [OOK] New comment added to issue id=%d: id_description=%d\n", id, claim.Id, desc.Id)
			if err = os.Remove(pathname); err != nil {
				log.Printf("%s [ERR] %s\n", id, err)
			}
			appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeamIssues, TeamId: team.Id})
		}
	} else {
		var exercice *fic.Exercice = nil
		if e, err := fic.GetExercice(issue.IdExercice); err == nil {
			exercice = e
		}

		if claim, err := fic.NewClaim(issue.Subject, team, exercice, nil, "medium"); err != nil {
			log.Printf("%s [ERR] Unable to create new issue: %s\n", id, err)
		} else if len(issue.Description) > 0 {
			if _, err := claim.AddDescription(issue.Description, &fic.ClaimAssignee{Id: 0}, true); err != nil {
				log.Printf("%s [WRN] Unable to add description to issue: %s\n", id, err)
			} else {
				log.Printf("%s [OOK] New issue created: id=%d\n", id, claim.Id)
				if err = os.Remove(pathname); err != nil {
					log.Printf("%s [ERR] %s\n", id, err)
				}
			}
		} else {
			log.Printf("%s [OOK] New issue created: id=%d\n", id, claim.Id)
			if err = os.Remove(pathname); err != nil {
				log.Printf("%s [ERR] %s\n", id, err)
			}
		}
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeamIssues, TeamId: team.Id})
	}
}
