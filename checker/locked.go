package main

import (
	"encoding/json"
	"log"
	"os"

	"srs.epita.fr/fic-server/libfic"
)

var TeamLockedExercices = map[int64]map[string]bool{}

func treatLocked(pathname string, team *fic.Team) {
	fd, err := os.Open(pathname)
	if err != nil {
		log.Printf("[ERR] Unable to open %q: %s", pathname, err)
		return
	}

	var locked map[string]bool

	jdec := json.NewDecoder(fd)
	if err := jdec.Decode(&locked); err != nil {
		log.Printf("[ERR] Unable to parse JSON %q: %s", pathname, err)
		return
	}

	TeamLockedExercices[team.Id] = locked
	log.Printf("Team %q (tid=%d) has locked %d exercices", team.Name, team.Id, len(locked))
}
