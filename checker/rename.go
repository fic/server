package main

import (
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"regexp"

	"srs.epita.fr/fic-server/libfic"
)

func validTeamName(name string) bool {
	match, err := regexp.MatchString("^[A-Za-z0-9 àéèêëîïôùûü_-]{1,32}$", name)
	return err == nil && match
}

func treatRename(pathname string, team *fic.Team) {
	// Generate a unique identifier to follow the request in logs
	bid := make([]byte, 5)
	binary.LittleEndian.PutUint32(bid, rand.Uint32())
	id := "[" + base64.StdEncoding.EncodeToString(bid) + "]"
	log.Println(id, "New renameTeam receive", pathname)

	var keys map[string]string

	if cnt_raw, err := ioutil.ReadFile(pathname); err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
	} else if err := json.Unmarshal(cnt_raw, &keys); err != nil {
		log.Printf("%s [ERR] %s\n", id, err)
	} else if validTeamName(keys["newName"]) {
		team.Name = keys["newName"]
		if _, err := team.Update(); err != nil {
			log.Printf("%s [WRN] Unable to change team name: %s\n", id, err)
		}
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
		if _, err := fic.NewEvent(fmt.Sprintf("Souhaitons bonne chance à l'équipe <strong>%s</strong> qui vient de nous rejoindre&#160;!", html.EscapeString(team.Name)), "info"); err != nil {
			log.Printf("%s [WRN] Unable to create event: %s\n", id, err)
		}
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenEvents})
		if err := os.Remove(pathname); err != nil {
			log.Printf("%s [ERR] %s\n", id, err)
		}
	}
}
