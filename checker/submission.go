package main

import (
	"encoding/base64"
	"encoding/binary"
	"encoding/json"
	"fmt"
	"html"
	"io/ioutil"
	"log"
	"math/rand"
	"os"
	"strconv"

	"golang.org/x/crypto/blake2b"

	"srs.epita.fr/fic-server/libfic"
)

type ResponsesUpload struct {
	Keys map[int]string `json:"flags"`
	MCQs map[int]bool   `json:"mcqs"`
	MCQJ map[int]string `json:"justifications"`
}

func treatSubmission(pathname string, team *fic.Team, exercice_id string) {
	// Generate a unique identifier to follow the request in logs
	bid := make([]byte, 5)
	binary.LittleEndian.PutUint32(bid, rand.Uint32())
	id := "[" + base64.StdEncoding.EncodeToString(bid) + "]"
	log.Println(id, "New submission receive", pathname)

	// Parse exercice_id argument
	eid, err := strconv.ParseInt(exercice_id, 10, 64)
	if err != nil {
		log.Printf("%s [ERR] %s is not a valid number: %s\n", id, exercice_id, err)
		return
	}

	// Identifier should not be blacklisted for the team
	if blacklistteam, ok := TeamLockedExercices[team.Id]; ok {
		if locked, ok := blacklistteam[exercice_id]; ok && locked {
			log.Printf("%s [!!!] Submission received for team's locked exercice %d\n", id, eid)
			return
		}
	}

	// Find the given exercice
	exercice, err := fic.GetExercice(eid)
	if err != nil {
		log.Printf("%s [ERR] Unable to find exercice %d: %s\n", id, eid, err)
		return
	}

	// Check the exercice is not disabled
	if exercice.Disabled {
		log.Println("[!!!] The team submits something for a disabled exercice")
		return
	}

	// Check the team can access this exercice
	if !team.HasAccess(exercice) {
		log.Println("[!!!] The team submits something for an exercice it doesn't have access yet")
		return
	}

	// Find the corresponding theme
	var theme *fic.Theme
	if exercice.IdTheme != nil {
		theme, err = fic.GetTheme(*exercice.IdTheme)
		if err != nil {
			log.Printf("%s [ERR] Unable to retrieve theme for exercice %d: %s\n", id, eid, err)
			return
		}

		// Theme should not be locked
		if theme.Locked {
			log.Printf("%s [!!!] Submission received for locked theme %d (eid=%d): %s\n", id, exercice.IdTheme, eid, theme.Name)
			return
		}
	}

	// Read received file
	cnt_raw, err := ioutil.ReadFile(pathname)
	if err != nil {
		log.Println(id, "[ERR] Unable to read file;", err)
		return
	}

	// Save checksum to treat duplicates
	cksum := blake2b.Sum512(cnt_raw)
	if err != nil {
		log.Println(id, "[ERR] JSON parsing error:", err)
		return
	}

	// Parse it
	var responses ResponsesUpload
	err = json.Unmarshal(cnt_raw, &responses)
	if err != nil {
		log.Println(id, "[ERR] JSON parsing error:", err)
		return
	}

	// Ensure the team didn't already solve this exercice
	tm := team.HasSolved(exercice)
	if tm != nil {
		if theme == nil {
			log.Printf("%s [WRN] Team %d ALREADY solved standalone exercice %d (%s), continuing for eventual bonus flags\n", id, team.Id, exercice.Id, exercice.Title)
		} else {
			log.Printf("%s [WRN] Team %d ALREADY solved exercice %d (%s : %s), continuing for eventual bonus flags\n", id, team.Id, exercice.Id, theme.Name, exercice.Title)
		}
	}

	// Handle MCQ justifications: convert to expected keyid
	for cid, j := range responses.MCQJ {
		if mcq, choice, err := fic.GetMCQbyChoice(cid); err != nil {
			log.Println(id, "[ERR] Unable to retrieve mcq from justification:", err)
			return
		} else if mcq.IdExercice != exercice.Id {
			log.Println(id, "[ERR] We retrieve an invalid MCQ: from exercice", mcq.IdExercice, "whereas expected from exercice", exercice.Id)
			return
		} else if key, err := choice.GetJustifiedFlag(exercice); err != nil {
			// Most probably, we enter here because the selected choice has not to be justified
			// So, just ignore this case as it will be invalidated by the mcq validation
			continue
		} else {
			if responses.Keys == nil {
				responses.Keys = map[int]string{}
			}
			responses.Keys[key.Id] = j
		}
	}

	// Check given answers
	solved, err := exercice.CheckResponse(cksum[:], responses.Keys, responses.MCQs, team)
	if err != nil {
		log.Println(id, "[ERR] Unable to CheckResponse:", err)
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
		return
	}

	// At this point, we have treated the file, so it can be safely deleted
	if err := os.Remove(pathname); err != nil {
		log.Println(id, "[ERR] Can't remove file:", err)
	}

	if tm != nil {
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
	} else if solved {
		if theme == nil {
			log.Printf("%s Team %d SOLVED exercice %d (%s)\n", id, team.Id, exercice.Id, exercice.Title)
		} else {
			log.Printf("%s Team %d SOLVED exercice %d (%s : %s)\n", id, team.Id, exercice.Id, theme.Name, exercice.Title)

		}
		if err := exercice.Solved(team); err != nil {
			log.Println(id, "[ERR] Unable to mark the challenge as solved:", err)
		}

		// Write event
		if theme == nil {
			if _, err := fic.NewEvent(fmt.Sprintf("L'équipe %s a résolu le défi %s&#160;!", html.EscapeString(team.Name), exercice.Title), "success"); err != nil {
				log.Println(id, "[WRN] Unable to create event:", err)
			}
		} else {
			if lvl, err := exercice.GetLevel(); err != nil {
				log.Println(id, "[ERR] Unable to get exercice level:", err)
			} else if _, err := fic.NewEvent(fmt.Sprintf("L'équipe %s a résolu le <strong>%d<sup>e</sup></strong> défi %s&#160;!", html.EscapeString(team.Name), lvl, theme.Name), "success"); err != nil {
				log.Println(id, "[WRN] Unable to create event:", err)
			}
		}
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenThemes})
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeams})
	} else {
		if theme == nil {
			log.Printf("%s Team %d submit an invalid solution for exercice %d (%s)\n", id, team.Id, exercice.Id, exercice.Title)
		} else {
			log.Printf("%s Team %d submit an invalid solution for exercice %d (%s : %s)\n", id, team.Id, exercice.Id, theme.Name, exercice.Title)
		}

		// Write event (only on first try)
		if tm == nil {
			if theme == nil {
				if _, err := fic.NewEvent(fmt.Sprintf("L'équipe %s tente le défi %s&#160;!", html.EscapeString(team.Name), exercice.Title), "warning"); err != nil {
					log.Println(id, "[WRN] Unable to create event:", err)
				}
			} else {
				if lvl, err := exercice.GetLevel(); err != nil {
					log.Println(id, "[ERR] Unable to get exercice level:", err)
				} else if _, err := fic.NewEvent(fmt.Sprintf("L'équipe %s tente le <strong>%d<sup>e</sup></strong> défi %s&#160;!", html.EscapeString(team.Name), lvl, theme.Name), "warning"); err != nil {
					log.Println(id, "[WRN] Unable to create event:", err)
				}
			}
		}
		appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenTeam, TeamId: team.Id})
	}

	appendGenQueue(fic.GenStruct{Id: id, Type: fic.GenEvents})
}
