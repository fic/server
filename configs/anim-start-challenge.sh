#!/bin/sh

echo "Launching challenge..."
echo

curl -s admin-challenge:8081/admin/api/settings.json | jq "{start:.start, end:.end}"

echo

for i in `seq 30 -1 1`
do
    echo -n "."
    sleep 0.036
done
echo

curl -s admin-challenge:8081/admin/api/settings.json | jq .

for i in `seq 40 -1 1`
do
    echo -n "."
    sleep 0.02
done
echo

echo "Challenge started!" | figlet
echo
echo
echo
