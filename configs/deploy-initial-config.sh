#!/bin/sh

set -e

echo "Perform initial config..."

[ -z "${PXE_IFACE}" ] && PXE_IFACE=$(ip r | grep default | head -1 | cut -d " " -f 5)
[ -z "${DEPLOY_NETWORK}" ] && DEPLOY_NETWORK=192.168.255

sed -i "s/eth0/${PXE_IFACE}/;s/192.168.255/${DEPLOY_NETWORK}/" /etc/udhcpd.conf

ip a show dev "${PXE_IFACE}" | grep "inet " | grep "${DEPLOY_NETWORK}" ||
    ip a add "${DEPLOY_NETWORK}.2/24" dev "${PXE_IFACE}"

echo "Initial config done."

[ -f fickit-boot-kernel ] || { echo "Missing fickit-boot-kernel file. Download the latest fickit artifact."; exit 1; }
[ -f fickit-metadata.iso ] || { echo "Missing fickit-metadata.iso file. Use configs/gen_metadata.sh to generate it."; exit 1; }

exec $@
