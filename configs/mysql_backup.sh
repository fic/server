#!/bin/sh

[ -z "${MYSQL_PASSWORD}" ] && [ -n "${MYSQL_PASSWORD_FILE}" ] && MYSQL_PASSWORD=$(cat "${MYSQL_PASSWORD_FILE}" | tr -d '\n')

while true
do
    sleep 360
    mysqldump -h "${MYSQL_HOST}" -u "${MYSQL_USER}" --password="${MYSQL_PASSWORD}" "${MYSQL_DATABASE}" | gzip > /var/lib/fic/backups/db-$(date +%Y%m%d%H%M).sql.gz
done
