server_tokens off;
proxy_cache_path  /var/cache/nginx levels=1:2 keys_zone=STATIC:10m inactive=24h max_size=1g;
proxy_connect_timeout 1s;

server {
        listen 80 default;
        listen [::]:80 default;

        include fic-auth.conf;

        root ${PATH_STATIC};

        error_page 401 /welcome.html;
        error_page 403 404 /e404.html;
        error_page 413 404 /e413.html;
        error_page 500 502 504 /e500.html;

        error_page 401 /welcome.html;
        error_page 403 404 /e404.html;
        error_page 413 /e413.html;
        error_page 500 502 504 /e500.html;

        add_header Strict-Transport-Security max-age=31536000;
        add_header X-Frame-Options deny;
	add_header Content-Security-Policy "script-src 'unsafe-inline' 'self' 'unsafe-eval'; img-src 'self' data:; style-src 'unsafe-inline' 'self'; font-src 'self'; default-src 'self'";
	add_header X-Xss-Protection "1; mode=block";
	add_header X-Content-Type-Options nosniff;
	add_header Referrer-Policy strict-origin;
	add_header Feature-Policy "accelerometer 'none'; ambient-light-sensor 'none'; autoplay 'none'; battery 'none'; camera 'none'; display-capture 'none'; document-domain 'none'; encrypted-media 'none'; fullscreen 'none'; geolocation 'none'; gyroscope 'none'; magnetometer 'none'; microphone 'none'; midi 'none'; payment 'none'; picture-in-picture 'none'; speaker 'none'; sync-xhr 'none'; usb 'none'; vr 'none'; wake-lock 'none'; xr-spatial-tracking 'none'";

        location = / {
            include fic-get-team.conf;
        }
        location = /index.html {
            include fic-get-team.conf;
        }
	location = /welcome.html {
	    internal;
	    if ($http_accept ~ "^application/json") {
	        rewrite ^/(.*).html$ /$1.json;
	    }
        }
	location = /e404.html {
	    internal;
	    if ($http_accept ~ "^application/json") {
	        rewrite ^/(.*).html$ /$1.json;
	    }
        }
	location = /e413.html {
	    internal;
	    if ($http_accept ~ "^application/json") {
	        rewrite ^/(.*).html$ /$1.json;
	    }
        }
	location = /e500.html {
	    internal;
	    if ($http_accept ~ "^application/json") {
	        rewrite ^/(.*).html$ /$1.json;
	    }
        }

        location ${FIC_BASEURL2} {
            rewrite ^${FIC_BASEURL2}(.*)$ /$1;
        }

        location ~ ^/([A-Z]|_/) {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }

        location /edit {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }
        location /issues {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }
        location /rank {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }
        location /tags/ {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }
        location /register {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }
        location /rules {
            include fic-get-team.conf;

            rewrite ^/.*$ /index.html;
        }

        location /files/ {
            alias       ${PATH_FILES}/;
	    sendfile    on;
            tcp_nodelay on;
            gzip_static  always;
        }

        location /wait.json {
	    include    fic-get-team.conf;

            root       ${PATH_TEAMS}/$team/;
            expires    epoch;
            add_header Cache-Control no-cache;
        }
        location /stats.json {
            root       ${PATH_TEAMS}/;
            expires    epoch;
            add_header Cache-Control no-cache;
        }
        location /my.json {
	    include    fic-get-team.conf;

            root       ${PATH_TEAMS}/$team/;
            expires    epoch;
            add_header Cache-Control no-cache;

            if (!-f ${PATH_STARTINGBLOCK}/started) {
                rewrite ^/ /wait.json;
            }
        }
        location /issues.json {
	    include    fic-get-team.conf;

            root ${PATH_TEAMS}/$team/;
            expires epoch;
            add_header Cache-Control no-cache;
        }
        location /scores.json {
	    include    fic-get-team.conf;

            root       ${PATH_TEAMS}/$team/;
            expires    epoch;
            add_header Cache-Control no-cache;
        }
        location /teams.json {
            root       ${PATH_TEAMS};
            expires    epoch;
            add_header Cache-Control no-cache;
        }
        location /themes.json {
	    include    fic-get-team.conf;

            root       ${PATH_TEAMS};
            expires    epoch;
            add_header Cache-Control no-cache;

            if (!-f ${PATH_TEAMS}/$team/my.json) {
                rewrite ^/ /themes-wait.json break;
            }
        }
        location /challenge.json {
            root       ${PATH_SETTINGS}/;
            expires    epoch;
	    add_header X-FIC-time $msec;
            add_header Cache-Control no-cache;
        }
        location /settings.json {
            root       ${PATH_SETTINGS}/;
            expires    epoch;
	    add_header X-FIC-time $msec;
            add_header Cache-Control no-cache;
        }

        location /submit/ {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_RECEIVER}/submission/;
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }
        location /issue {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_RECEIVER};
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }
        location /chname {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_RECEIVER};
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }
        location /registration {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_RECEIVER};
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }
        location /reset_progress {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_RECEIVER};
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }
	location /openhint/ {
	    include    fic-get-team.conf;

	    proxy_pass        http://${HOST_RECEIVER};
	    proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
	    proxy_redirect    off;
	}
	location /wantchoices/ {
	    include    fic-get-team.conf;

	    proxy_pass        http://${HOST_RECEIVER};
	    proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
	    proxy_redirect    off;
	}

        location /api {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_ADMIN}${FIC_BASEURL}admin/api;
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }

        location ${FIC_BASEURL}admin {
            proxy_pass        http://${HOST_ADMIN};
            proxy_set_header  X-Forwarded-For  $remote_addr;
            proxy_redirect    off;
        }

        location ${FIC_BASEURL}admin/api {
            proxy_pass        http://${HOST_ADMIN};
            proxy_read_timeout 400s;
            proxy_set_header  X-Forwarded-For  $remote_addr;
        }

        location ${FIC_BASEURL}qa {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_QA};
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }

        location ${FIC_BASEURL}dashboard {
            include fic-get-team.conf;

            proxy_pass        http://${HOST_DASHBOARD};
            proxy_set_header  X-Forwarded-For  $remote_addr;
	    proxy_set_header  X-FIC-Team       $team;
            proxy_redirect    off;
        }

        location = /events.json {
            proxy_pass        http://${HOST_ADMIN}/api/events/;
            proxy_method      GET;
            proxy_pass_request_body off;
            proxy_set_header  Content-Length "";
            proxy_set_header  X-Forwarded-For  $remote_addr;
            proxy_redirect    off;
            proxy_cache       STATIC;
            proxy_cache_valid 3s;
        }
}
