#!/bin/sh

if [ -d /containers/onboot/006-synchro-ip-setup ]; then
    LOWER=/containers/onboot/006-synchro-ip-setup/lower
elif [ -d /containers/onboot/006-nginx-ip-setup ]; then
    LOWER=/containers/onboot/006-nginx-ip-setup/lower
else
    nsenter -t 1 -m -u -i -p -- "$0" $@
    exit $?
fi

mount -t tmpfs none $LOWER/run

chroot $LOWER iptables $@
EXIT=$?

umount $LOWER/run

exit ${EXIT}
