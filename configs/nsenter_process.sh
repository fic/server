#!/bin/sh

PROC="$1"
shift

nsenter -t $(pgrep "$PROC" | head -1) $@
exit $?
