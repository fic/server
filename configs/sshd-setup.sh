#!/bin/sh

[ -f /var/lib/fic/ssh/sshd_config ] && exit 0

mkdir -p /var/lib/fic/ssh/

cp /containers/services/sshd/rootfs/etc/ssh/* /var/lib/fic/ssh/

mount -o bind /dev /containers/services/sshd/rootfs/dev
mount -o bind /proc /containers/services/sshd/rootfs/proc
mount -o bind /sys /containers/services/sshd/rootfs/sys
mount -o bind /var/lib/fic/ssh/ /containers/services/sshd/rootfs/etc/ssh

chroot /containers/services/sshd/rootfs/ ssh-keygen -A

umount /containers/services/sshd/rootfs/dev /containers/services/sshd/rootfs/proc /containers/services/sshd/rootfs/sys /containers/services/sshd/rootfs/etc/ssh
