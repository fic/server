#!/bin/sh

IP_BACKEND=192.168.3.92
IMG_BACKEND=fickit-backend-squashfs.img
IMG_METADATA=fickit-metadata.iso

echo "Sending image..."
rsync -v -e ssh "${IMG_BACKEND}" "${IMG_METADATA}" "root@${IP_BACKEND}:/var/lib/fic/outofsync/" || exit 1

echo "Done!"
echo "Now, execute upgrade_image on backend, through iDRAC interface."
