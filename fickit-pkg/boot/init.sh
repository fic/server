#!/bin/sh

# /proc/cmdline parser (from Gentoo Wiki)
cmdline() {
    local value
    value=" $(cat /proc/cmdline) "
    value="${value##* $1=}"
    value="${value%% *}"
    [ "$value" != "" ] && echo "$value"
}

modprobe xhci_pci
modprobe ahci
modprobe intel_lpss_pci
modprobe i2c_i801
modprobe megaraid_sas
modprobe tg3
modprobe bnxt_en

mount -t devtmpfs none /dev
mount -t proc none /proc

mount -o ro /dev/sda1 /boot

mount -o loop -t squashfs /boot/imgs/$(cmdline root) /mnt || /bin/sh

umount /dev
umount /proc
mount --move /boot /mnt/boot

# Switch
exec switch_root /mnt /init
