{
  description = "Submission server/infrastructure for the SRS challenge at FIC";

  inputs.nixpkgs.url = "nixpkgs/nixos-unstable";

  inputs.flake-compat = {
    url = "github:edolstra/flake-compat";
    flake = false;
  };

  outputs = { self, nixpkgs, ... }:
    let

      # Generate a version based on date
      version = builtins.substring 0 12 self.lastModifiedDate;
      vendorSha256 = "sha256-itCvN/Z8DkUUdtx6At+4DyeJK8PgFJ/5A3G03VT4I2k";
      overrideModAttrs = _ : { name = "fic-./.-${version}-go-modules"; };

      # System types to support.
      supportedSystems =
        [ "x86_64-linux" "x86_64-darwin" "aarch64-linux" "aarch64-darwin" "arm-linux" ];

      # Helper function to generate an attrset '{ x86_64-linux = f "x86_64-linux"; ... }'.
      forAllSystems = nixpkgs.lib.genAttrs supportedSystems;

      # Nixpkgs instantiated for supported system types.
      nixpkgsFor = forAllSystems (system: import nixpkgs { inherit system; });

    in {

      # Provide some binary packages for selected system types.
      packages = forAllSystems (system:
        let pkgs = nixpkgsFor.${system};
        in {
          fic-admin = pkgs.buildGoModule {
            pname = "admin";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "admin" ];
          };

          fic-checker = pkgs.buildGoModule {
            pname = "checker";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "checker" ];
          };

          fic-dashboard = pkgs.buildGoModule {
            pname = "dashboard";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "dashboard" ];
          };

          fic-generator = pkgs.buildGoModule {
            pname = "generator";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "generator" ];
          };

          fic-synchro = pkgs.writeShellApplication {
            name = "synchro";
            runtimeInputs = [ pkgs.rsync pkgs.openssh pkgs.coreutils ];
            text = ''
              ${(builtins.readFile ./configs/synchro.sh)}
            '';
          };

          fic-configs = pkgs.stdenv.mkDerivation {
            name = "configs";
            src = ./.;
            installPhase = "mkdir -p $out/; cp -r configs/ $out/";
          };

          fic-receiver = pkgs.buildGoModule {
            pname = "receiver";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "receiver" ];
          };

          fic-qa = pkgs.buildGoModule {
            pname = "qa";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "qa" ];
          };

          fic-remote-scores-sync-zqds = pkgs.buildGoModule {
            pname = "scores-sync-zqds";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "remote/scores-sync-zqds" ];
          };

          fic-remote-challenge-sync-airbus = pkgs.buildGoModule {
            pname = "challenge-sync-airbus";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "remote/challenge-sync-airbus" ];
          };

          fic-repochecker = pkgs.buildGoModule {
            pname = "repochecker";
            inherit version vendorSha256 overrideModAttrs;
            src = ./.;

            subPackages = [ "repochecker" ];
          };
          all = pkgs.linkFarmFromDrvs "fic-all" (builtins.attrValues (builtins.removeAttrs self.packages.x86_64-linux [ "all" ]));
        });

      devShell = forAllSystems (system:
        let pkgs = nixpkgsFor.${system};
        in pkgs.mkShell {
          buildInputs = with pkgs; [ go ];
        });
    };
}
