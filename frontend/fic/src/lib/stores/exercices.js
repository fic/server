import { derived, writable } from 'svelte/store';

import { exercices_idx_urlid } from './themes';

export const set_current_exercice = writable(null)

export const current_exercice = derived(
  [set_current_exercice, exercices_idx_urlid],
  ([$set_current_exercice, $exercices_idx_urlid]) => {
    if ($exercices_idx_urlid === null || Object.keys($exercices_idx_urlid).length == 0) {
      return null;
    }

    if ($exercices_idx_urlid[$set_current_exercice])
      return $exercices_idx_urlid[$set_current_exercice];

    return undefined;
  }
)
