import { derived, writable } from 'svelte/store';

import { stop_refresh } from './common';

let refresh_interval_themes = null;

function createThemesStore() {
  const { subscribe, set, update } = writable({});

  async function updateFunc (res_themes, cb=null) {
    if (res_themes.status === 200) {
      const themes = await res_themes.json();

      update((t) => themes);

      if (cb) {
        cb(themes);
      }
    }
  }

  async function refreshFunc(cb=null, interval=null) {
    if (refresh_interval_themes)
      clearInterval(refresh_interval_themes);
    if (interval === null) {
      interval = Math.floor(Math.random() * 24000) + 32000;
    }
    if (stop_refresh.state) {
      return;
    }
    refresh_interval_themes = setInterval(refreshFunc, interval);

    await updateFunc(await fetch('themes.json', {headers: {'Accept': 'application/json'}}), cb);
  }

  return {
    subscribe,

    refresh: refreshFunc,

    update: updateFunc,
  };
}

export const themesStore = createThemesStore();

export const themes = derived(
  themesStore,
  ($themesStore) => {
    const themes = {};

    for (const key in $themesStore) {
      const theme = $themesStore[key];

      themes[key] = theme
      themes[key].exercice_count = theme.exercices.length;
      themes[key].exercice_coeff_max = 0;
      themes[key].max_gain = 0;

      for (const k in theme.exercices) {
        const exercice = theme.exercices[k];

        themes[key].max_gain += exercice.gain;
        if (themes[key].exercice_coeff_max < exercice.curcoeff) {
          themes[key].exercice_coeff_max = exercice.curcoeff;
        }

	if (k > 0)
	  themes[key].exercices[k-1].next = k;
      }
    }

    return themes;
  },
);

export const themes_idx = derived(
  themes,
  ($themes) => {
    const ret = {};

    for (const key in $themes) {
      const theme = $themes[key];

      ret[theme.urlid] = theme;
    }

    return ret;
  },
  null,
);

export const exercices_idx = derived(
  themesStore,
  ($themesStore) => {
    const ret = {};

    for (const key in $themesStore) {
      const theme = $themesStore[key];
      for (let exercice of theme.exercices) {
        ret[exercice.id] = exercice;
        ret[exercice.id].id_theme = key;
      }
    }

    return ret;
  },
);

export const exercices_idx_urlid = derived(
  themesStore,
  ($themesStore) => {
    const ret = {};

    for (const key in $themesStore) {
      const theme = $themesStore[key];
      for (let exercice of theme.exercices) {
        ret[exercice.urlid] = exercice;
      }
    }

    return ret;
  },
  null
);

export const max_solved = derived(
  themesStore,
  ($themesStore) => {
    let ret = 0;

    for (const key in $themesStore) {
      const theme = $themesStore[key];
      if (theme.solved > ret) {
        ret = theme.solved;
      }
    }

    return ret;
  },
);

export const set_current_theme = writable(null)

export const current_theme = derived(
  [set_current_theme, themes_idx],
  ([$set_current_theme, $themes_idx]) => {
    if ($themes_idx === null || Object.keys($themes_idx).length == 0) {
      return null;
    }

    if ($themes_idx[$set_current_theme])
      return $themes_idx[$set_current_theme];

    return undefined;
  }
)
