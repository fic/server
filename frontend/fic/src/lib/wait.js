import { writable } from 'svelte/store';

import { my } from '$lib/stores/my.js';
import { teamsStore } from '$lib/stores/teams.js';

export let waitInProgress = writable(false);
export let timeouted = writable(false);

export function waitDiff(i, exercice) {
  timeouted.set(false);
  my.refresh((my) => {
    if (my && (my.exercices[exercice.id].tries != exercice.tries || my.exercices[exercice.id].solved_rank != exercice.solved_rank || my.exercices[exercice.id].solved_time != exercice.solved_time)) {
      waitInProgress.set(false);
      teamsStore.refresh();
    } else if (i > 0) {
      setTimeout(waitDiff, (12-i)*50+440, i-1, exercice);
    } else {
      timeouted.set(true);
      waitInProgress.set(false);
    }
  })
}
