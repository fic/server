import { challengeInfo } from '$lib/stores/challengeinfo.js';
import { stop_refresh } from '$lib/stores/common';
import { issuesStore } from '$lib/stores/issues.js';
import { my } from '$lib/stores/my.js';
import { teamsStore } from '$lib/stores/teams.js';
import { themesStore } from '$lib/stores/themes.js';
import { settings, time } from '$lib/stores/settings.js';

export const ssr = false;

export async function load() {
  await challengeInfo.refresh();
  await settings.refresh();
  await themesStore.refresh();
  teamsStore.refresh();
  my.refresh((my) => {
    if (my && my.team_id === 0) {
      stop_refresh.state = true;
    }
  });
  issuesStore.refresh();
}
