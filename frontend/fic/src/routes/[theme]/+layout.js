import { set_current_theme } from '$lib/stores/themes';

export function load({ params }) {
  set_current_theme.set(params.theme);
}
