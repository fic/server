import { set_current_exercice } from '$lib/stores/exercices';

export function load({ params }) {
  set_current_exercice.set(params.exercice);
}
