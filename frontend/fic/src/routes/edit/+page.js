import { set_current_theme } from '$lib/stores/themes';

export function load() {
  set_current_theme.set(null);
}
