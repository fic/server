import { get_store_value } from 'svelte/internal';

import { exercices_idx } from '$lib/stores/themes.js';

export async function load({ url }) {
  const eidx = get_store_value(exercices_idx);

  const exercice = eidx[url.searchParams.get("eid")]?eidx[url.searchParams.get("eid")]:null;

  return {
    exercice: exercice,
    fillIssue: exercice !== null || url.searchParams.get("fill-issue") !== null,
  };
}
