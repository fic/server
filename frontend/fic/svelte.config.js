/** @type {import('@sveltejs/kit').Config} */
import adapt from '@sveltejs/adapter-static';

const config = {
	kit: {
		adapter: adapt({
			fallback: 'index.html'
		}),
                paths: {
                        relative: true
                },
	}
};

export default config;
