package fic

import ()

type Flag interface {
	GetId() int
	RecoverId() (Flag, error)
	Create(e *Exercice) (Flag, error)
	Update() (int64, error)
	Delete() (int64, error)
	AddDepend(d Flag) error
	GetDepends() ([]Flag, error)
	GetOrder() int8
	Check(val interface{}) int
	IsOptionnal() bool
	FoundBy(t *Team) error
	NbTries() (int64, error)
	TeamsOnIt() ([]int64, error)
	DeleteTries() error
}

// GetFlag returns a list of flags comming with the challenge.
func (e *Exercice) GetFlags() ([]Flag, error) {
	var flags []Flag

	if ks, err := e.GetFlagKeys(); err != nil {
		return nil, err
	} else {
		for _, k := range ks {
			flags = append(flags, k)
		}
	}

	if ls, err := e.GetFlagLabels(); err != nil {
		return nil, err
	} else {
		for _, l := range ls {
			flags = append(flags, l)
		}
	}

	if ms, err := e.GetMCQ(); err != nil {
		return nil, err
	} else {
		for _, m := range ms {
			flags = append(flags, m)
		}
	}

	return flags, nil
}

// AddFlag add the given flag and eventually its entries (from MCQ).
func (e *Exercice) AddFlag(flag Flag) (Flag, error) {
	return flag.Create(e)
}

// WipeFlags deletes flags coming with the challenge.
func (e *Exercice) WipeFlags() (int64, error) {
	if _, err := DBExec("DELETE FROM exercice_files_okey_deps WHERE id_flag IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_files_omcq_deps WHERE id_mcq IN (SELECT id_mcq FROM exercice_mcq WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_flags_deps WHERE id_flag IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?) OR id_flag_dep IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id, e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_flags_omcq_deps WHERE id_flag IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_flag_labels_omcq_deps WHERE id_label IN (SELECT id_label FROM exercice_flag_labels WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_flag_labels_deps WHERE id_label IN (SELECT id_label FROM exercice_flag_labels WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_hints_okey_deps WHERE id_flag_dep IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_hints_omcq_deps WHERE id_mcq_dep IN (SELECT id_mcq FROM exercice_mcq WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_mcq_okey_deps WHERE id_flag_dep IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM team_wchoices WHERE id_flag IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM flag_choices WHERE id_flag IN (SELECT id_flag FROM exercice_flags WHERE id_exercice = ?)", e.Id); err != nil {
		return 0, err
	} else if _, err := DBExec("DELETE FROM exercice_flag_labels WHERE id_exercice = ?", e.Id); err != nil {
		return 0, err
	} else if res, err := DBExec("DELETE FROM exercice_flags WHERE id_exercice = ?", e.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
