package fic

type GenerateType int

const (
	GenPublic GenerateType = iota
	GenEvents
	GenTeam
	GenTeams
	GenThemes
	GenTeamIssues
)

type GenStruct struct {
	Id     string       `json:"id"`
	Type   GenerateType `json:"type"`
	TeamId int64        `json:"team_id,omitempty"`
	ended  chan error
}

func (gs *GenStruct) GenEnded() chan error {
	gs.ended = make(chan error, 1)
	return gs.ended
}

func (gs *GenStruct) GetEnded() chan error {
	return gs.ended
}

func (gs *GenStruct) End(err error) {
	if gs.ended != nil {
		gs.ended <- err
	}
}
