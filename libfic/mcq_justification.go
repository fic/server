package fic

import (
	"errors"
	"strconv"
	"strings"
)

type FlagMCQLabel struct {
	Label    string
	IdChoice int
}

// IsMCQJustification tells you if this key represent a justification from a MCQ.
func (k FlagKey) IsMCQJustification() bool {
	return len(k.Label) > 0 && k.Label[0] == '%'
}

// GetMCQJustification returns the structure corresponding to the given flag.
func (k FlagKey) GetMCQJustification() (fl FlagMCQLabel, err error) {
	spl := strings.Split(k.Label, "%")
	if len(spl) >= 3 && len(spl[0]) == 0 {
		var idChoice int64
		idChoice, err = strconv.ParseInt(spl[1], 10, 32)
		fl.IdChoice = int(idChoice)
		fl.Label = strings.Join(spl[2:], "%")
	} else {
		err = errors.New("this is not a MCQ justification")
	}
	return
}
