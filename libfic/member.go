package fic

import ()

// Member represents a team member
type Member struct {
	Id        int64  `json:"id"`
	Firstname string `json:"firstname"`
	Lastname  string `json:"lastname"`
	Nickname  string `json:"nickname"`
	Company   string `json:"company"`
}

// GetMembers retrieves the members of the Team
func (t *Team) GetMembers() ([]*Member, error) {
	if rows, err := DBQuery("SELECT id_member, firstname, lastname, nickname, company FROM team_members WHERE id_team = ?", t.Id); err != nil {
		return nil, err
	} else {
		defer rows.Close()

		var members []*Member
		for rows.Next() {
			m := &Member{}
			if err := rows.Scan(&m.Id, &m.Firstname, &m.Lastname, &m.Nickname, &m.Company); err != nil {
				return nil, err
			}
			members = append(members, m)
		}
		if err := rows.Err(); err != nil {
			return nil, err
		}

		return members, nil
	}
}

// AddMember creates and fills a new struct Member and registers it into the database.
func (t *Team) AddMember(firstname string, lastname string, nickname string, company string) (*Member, error) {
	if res, err := DBExec("INSERT INTO team_members (id_team, firstname, lastname, nickname, company) VALUES (?, ?, ?, ?, ?)", t.Id, firstname, lastname, nickname, company); err != nil {
		return nil, err
	} else if mid, err := res.LastInsertId(); err != nil {
		return nil, err
	} else {
		return &Member{mid, firstname, lastname, nickname, company}, nil
	}
}

// GainMember associates (or registers, it if it doesn't exists yet) a member to the team.
func (t *Team) GainMember(m *Member) error {
	if m.Id == 0 {
		if res, err := DBExec("INSERT INTO team_members (id_team, firstname, lastname, nickname, company) VALUES (?, ?, ?, ?, ?)", t.Id, m.Firstname, m.Lastname, m.Nickname, m.Company); err != nil {
			return err
		} else if _, err := res.LastInsertId(); err != nil {
			return err
		} else {
			return nil
		}
	} else if res, err := DBExec("UPDATE team_members SET id_team = ? WHERE id_member = ?", t.Id, m.Id); err != nil {
		return err
	} else if _, err := res.RowsAffected(); err != nil {
		return err
	} else {
		return nil
	}
}

// Update applies modifications back to the database.
func (m *Member) Update() (int64, error) {
	if res, err := DBExec("UPDATE team_members SET firstname = ?, lastname = ?, nickname = ?, company = ? WHERE id_member = ?", m.Firstname, m.Lastname, m.Nickname, m.Company, m.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

// Delete the member from the database.
func (m *Member) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM team_members WHERE id_member = ?", m.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

// ClearMembers deletes members in the team.
func (t *Team) ClearMembers() (int64, error) {
	if res, err := DBExec("DELETE FROM team_members WHERE id_team = ?", t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}
