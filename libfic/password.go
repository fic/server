package fic

import (
	"crypto/rand"
	"encoding/base64"
	"strings"
)

func GeneratePassword() (password string, err error) {
	// This will make a 12 chars long password
	b := make([]byte, 9)

	if _, err = rand.Read(b); err != nil {
		return
	}

	password = base64.StdEncoding.EncodeToString(b)

	// Avoid hard to read characters
	for _, i := range [][2]string{
		{"v", "*"}, {"u", "("},
		{"l", "%"}, {"1", "?"},
		{"o", "@"}, {"O", "!"}, {"0", ">"},
		// This one is to avoid problem with openssl
		{"/", "^"},
	} {
		password = strings.Replace(password, i[0], i[1], -1)
	}

	return
}
