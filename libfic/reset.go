package fic

import (
	"context"
)

// truncateTable performs an insecure wipe on the given tables.
func truncateTable(tables ...string) error {
	if tx, err := db.BeginTx(context.TODO(), nil); err != nil {
		return err
	} else {
		if _, err := tx.Exec("SET FOREIGN_KEY_CHECKS = 0;"); err != nil {
			return err
		}
		for _, table := range tables {
			if _, err := tx.Exec("TRUNCATE TABLE " + table + ";"); err != nil {
				return err
			}
		}
		if _, err := tx.Exec("SET FOREIGN_KEY_CHECKS = 1;"); err != nil {
			return err
		}
		if err := tx.Commit(); err != nil {
			return err
		}
	}
	return nil
}

// ResetAnnexes resets all tables containing annexe info like events, claims and qa.
func ResetAnnexes() error {
	return truncateTable(
		"claim_descriptions",
		"claims",
		"events",
		"exercices_qa",
		"qa_comments",
		"teams_qa_todo",
		"teams_qa_view",
	)
}

// ResetGame resets all tables containing team attempts and solves.
func ResetGame() error {
	return truncateTable(
		"team_wchoices",
		"team_hints",
		"flag_found",
		"mcq_found",
		"exercice_solved",
		"exercice_tries",
	)
}

// ResetExercices wipes out all challenges (both attempts and statements).
func ResetExercices() error {
	return truncateTable(
		"team_wchoices",
		"team_hints",
		"exercice_files_okey_deps",
		"exercice_files_omcq_deps",
		"exercice_files",
		"flag_found",
		"exercice_flags_omcq_deps",
		"exercice_mcq_okey_deps",
		"exercice_mcq_omcq_deps",
		"exercice_flags_deps",
		"exercice_hints_okey_deps",
		"exercice_hints_omcq_deps",
		"flag_choices",
		"exercice_flag_labels_omcq_deps",
		"exercice_flag_labels_deps",
		"exercice_flag_labels",
		"exercice_flags",
		"exercice_solved",
		"exercice_tries",
		"exercice_hints",
		"mcq_found",
		"mcq_entries",
		"exercice_mcq",
		"exercice_tags",
		"exercices",
		"themes",
	)
}

// ResetTeams wipes out all teams, incluings members and attempts.
func ResetTeams() error {
	return truncateTable(
		"team_wchoices",
		"team_hints",
		"flag_found",
		"mcq_found",
		"exercice_solved",
		"exercice_tries",
		"team_members",
		"teams",
	)
}
