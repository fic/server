package fic

import (
	"testing"
)

func TestReverseTriesPoints(t *testing.T) {
	for _, i := range []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} {
		if ReverseTriesPoints(i) != 10+i {
			t.Fatalf("Expected %d, got %d", 10+i, ReverseTriesPoints(i))
		}
	}

	for _, i := range []int64{1, 2, 3, 4, 5, 6, 7, 8, 9} {
		if ReverseTriesPoints(10+i*2) != 20+i {
			t.Fatalf("Expected %d, got %d", 20+i, ReverseTriesPoints(10+i*2))
		}
	}
}

func TestTermTriesSeq(t *testing.T) {
	for _, j := range []float64{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10} {
		for _, i := range []int64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10} {
			if TermTriesSeq(int64(10*j)+i) != j {
				t.Fatalf("Term %d: Expected %f, got %f", int64(10*j)+i, j, TermTriesSeq(i))
			}
		}
	}
}
