package fic

import (
	"fmt"
)

// exportedTeam is a structure representing a Team, as exposed to players.
type ExportedTeam struct {
	Name       string    `json:"name"`
	Color      string    `json:"color"`
	Rank       int       `json:"rank"`
	Points     float64   `json:"score"`
	Members    []*Member `json:"members,omitempty"`
	ExternalId string    `json:"external_id,omitempty"`
}

// Exportedteam creates the structure to respond as teams.json.
func ExportTeams(includeMembers bool) (ret map[string]ExportedTeam, err error) {
	var teams []*Team
	var rank map[int64]int

	if teams, err = GetTeams(); err != nil {
		return
	} else if rank, err = GetRank(); err != nil {
		return nil, err
	} else {
		ret = map[string]ExportedTeam{}
		for _, team := range teams {
			points, _ := team.GetPoints()
			var members []*Member
			if includeMembers {
				if members, err = team.GetMembers(); err != nil {
					return
				}
			}
			ret[fmt.Sprintf("%d", team.Id)] = ExportedTeam{
				team.Name,
				fmt.Sprintf("#%06x", team.Color),
				rank[team.Id],
				points * GlobalScoreCoefficient,
				members,
				team.ExternalId,
			}
		}

		return
	}
}
