package fic

import (
	"time"
)

// GetHistory aggregates all sources of events or actions for a Team
func (t *Team) GetHistory() ([]map[string]interface{}, error) {
	hist := make([]map[string]interface{}, 0)

	if rows, err := DBQuery(`SELECT id_team, "tries" AS kind, time, 0, E.id_exercice, E.title, NULL, NULL FROM exercice_tries T INNER JOIN exercices E ON E.id_exercice = T.id_exercice WHERE id_team = ? UNION
                                 SELECT id_team, "solved" AS kind, time, coefficient, E.id_exercice, E.title, NULL, NULL FROM exercice_solved S INNER JOIN exercices E ON E.id_exercice = S.id_exercice WHERE id_team = ? UNION
                                 SELECT id_team, "hint" AS kind, time, coefficient, E.id_exercice, E.title, H.id_hint, H.title FROM team_hints T INNER JOIN exercice_hints H ON H.id_hint = T.id_hint INNER JOIN exercices E ON E.id_exercice = H.id_exercice WHERE id_team = ? UNION
                                 SELECT id_team, "wchoices" AS kind, time, coefficient, E.id_exercice, E.title, F.id_flag, F.type FROM team_wchoices W INNER JOIN exercice_flags F ON F.id_flag = W.id_flag INNER JOIN exercices E ON E.id_exercice = F.id_exercice WHERE id_team = ? UNION
                                 SELECT id_team, "flag_found" AS kind, time, 0, E.id_exercice, E.title, K.id_flag, K.type FROM flag_found F INNER JOIN exercice_flags K ON K.id_flag = F.id_flag INNER JOIN exercices E ON K.id_exercice = E.id_exercice WHERE id_team = ? UNION
                                 SELECT id_team, "mcq_found" AS kind, time, 0, E.id_exercice, E.title, Q.id_mcq, Q.title FROM mcq_found F INNER JOIN exercice_mcq Q ON Q.id_mcq = F.id_mcq INNER JOIN exercices E ON Q.id_exercice = E.id_exercice WHERE id_team = ?
                                 ORDER BY time DESC`, t.Id, t.Id, t.Id, t.Id, t.Id, t.Id); err != nil {
		return nil, err
	} else {
		defer rows.Close()

		for rows.Next() {
			var id_team int64
			var kind string
			var time time.Time
			var coefficient float32
			var primary *int64
			var primary_title *string
			var secondary *int64
			var secondary_title *string

			if err := rows.Scan(&id_team, &kind, &time, &coefficient, &primary, &primary_title, &secondary, &secondary_title); err != nil {
				return nil, err
			}

			h := map[string]interface{}{}

			h["kind"] = kind
			h["time"] = time
			h["coefficient"] = coefficient
			if primary != nil {
				h["primary"] = primary
				h["primary_title"] = primary_title
			}
			if secondary != nil {
				h["secondary"] = secondary
				h["secondary_title"] = secondary_title
			}

			hist = append(hist, h)
		}
	}

	return hist, nil
}

// UpdateHistoryCoeff updates the coefficient for a given entry.
func (t *Team) UpdateHistoryCoeff(kind string, h time.Time, givenId int64, newCoeff float32) (int64, error) {
	if kind == "hint" {
		if res, err := DBExec("UPDATE team_hints SET coefficient = ? WHERE id_team = ? AND time = ? AND id_hint = ?", newCoeff, t.Id, h, givenId); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "wchoices" {
		if res, err := DBExec("UPDATE team_wchoices SET coefficient = ? WHERE id_team = ? AND time = ? AND id_flag = ?", newCoeff, t.Id, h, givenId); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "solved" {
		if res, err := DBExec("UPDATE exercice_solved SET coefficient = ? WHERE id_team = ? AND time = ? AND id_exercice = ?", newCoeff, t.Id, h, givenId); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else {
		return 0, nil
	}
}

// DelHistoryItem removes from the database an entry from the history.
func (t *Team) DelHistoryItem(kind string, h time.Time, primary *int64, secondary *int64) (interface{}, error) {
	if kind == "tries" && primary != nil {
		if res, err := DBExec("DELETE FROM exercice_tries WHERE id_team = ? AND time = ? AND id_exercice = ?", t.Id, h, *primary); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "hint" && primary != nil && secondary != nil {
		if res, err := DBExec("DELETE FROM team_hints WHERE id_team = ? AND time = ? AND id_hint = ?", t.Id, h, *secondary); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "wchoices" && primary != nil && secondary != nil {
		if res, err := DBExec("DELETE FROM team_wchoices WHERE id_team = ? AND time = ? AND id_flag = ?", t.Id, h, *secondary); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "flag_found" && primary != nil && secondary != nil {
		if res, err := DBExec("DELETE FROM flag_found WHERE id_team = ? AND time = ? AND id_flag = ?", t.Id, h, *secondary); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "mcq_found" && primary != nil && secondary != nil {
		if res, err := DBExec("DELETE FROM mcq_found WHERE id_team = ? AND time = ? AND id_mcq = ?", t.Id, h, *secondary); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else if kind == "solved" && primary != nil {
		if res, err := DBExec("DELETE FROM exercice_solved WHERE id_team = ? AND time = ? AND id_exercice = ?", t.Id, h, *primary); err != nil {
			return 0, err
		} else if nb, err := res.RowsAffected(); err != nil {
			return 0, err
		} else {
			return nb, err
		}
	} else {
		return nil, nil
	}
}

// ResetProgressionOnExercice removes all tries and validations for a given Exercice.
func (t *Team) ResetProgressionOnExercice(exercice *Exercice) error {
	hints, err := exercice.GetHints()
	if err != nil {
		return err
	}

	flags, err := exercice.GetFlags()
	if err != nil {
		return err
	}

	if _, err := DBExec("DELETE FROM exercice_tries WHERE id_team = ? AND id_exercice = ?", t.Id, exercice.Id); err != nil {
		return err
	}
	if _, err := DBExec("DELETE FROM exercice_solved WHERE id_team = ? AND id_exercice = ?", t.Id, exercice.Id); err != nil {
		return err
	}

	for _, hint := range hints {
		if _, err := DBExec("DELETE FROM team_hints WHERE id_team = ? AND id_hint = ?", t.Id, hint.Id); err != nil {
			return err
		}

	}

	for _, flag := range flags {
		if k, ok := flag.(*FlagKey); ok {
			if _, err := DBExec("DELETE FROM team_wchoices WHERE id_team = ? AND id_flag = ?", t.Id, k.Id); err != nil {
				return err
			}
			if _, err := DBExec("DELETE FROM flag_found WHERE id_team = ? AND id_flag = ?", t.Id, k.Id); err != nil {
				return err
			}
		} else if mcq, ok := flag.(*MCQ); ok {
			if _, err := DBExec("DELETE FROM mcq_found WHERE id_team = ? AND id_mcq = ?", t.Id, mcq.Id); err != nil {
				return err
			}
		}
	}

	return nil
}
