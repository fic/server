package fic

import ()

// Theme represents a group of challenges, to display to players
type Theme struct {
	Id              int64  `json:"id"`
	Language        string `json:"lang,omitempty"`
	Name            string `json:"name"`
	Locked          bool   `json:"locked"`
	URLId           string `json:"urlid"`
	Path            string `json:"path"`
	Authors         string `json:"authors,omitempty"`
	Intro           string `json:"intro,omitempty"`
	Headline        string `json:"headline,omitempty"`
	Image           string `json:"image,omitempty"`
	BackgroundColor uint32 `json:"background_color,omitempty"`
	PartnerImage    string `json:"partner_img,omitempty"`
	PartnerLink     string `json:"partner_href,omitempty"`
	PartnerText     string `json:"partner_txt,omitempty"`
}

func (t *Theme) GetId() *int64 {
	if t.Id == 0 {
		return nil
	}

	return &t.Id
}

// CmpTheme returns true if given Themes are identicals.
func CmpTheme(t1 *Theme, t2 *Theme) bool {
	return t1 != nil && t2 != nil && !(t1.Name != t2.Name || t1.URLId != t2.URLId || t1.Path != t2.Path || t1.Authors != t2.Authors || t1.Intro != t2.Intro || t1.Headline != t2.Headline || t1.Image != t2.Image || t1.BackgroundColor != t2.BackgroundColor || t1.PartnerImage != t2.PartnerImage || t1.PartnerLink != t2.PartnerLink || t1.PartnerText != t2.PartnerText)
}

// GetThemes returns a list of registered Themes from the database.
func GetThemes() ([]*Theme, error) {
	if rows, err := DBQuery("SELECT id_theme, name, locked, url_id, path, authors, intro, headline, image, background_color, partner_img, partner_href, partner_text FROM themes"); err != nil {
		return nil, err
	} else {
		defer rows.Close()

		var themes []*Theme
		for rows.Next() {
			t := &Theme{}
			if err := rows.Scan(&t.Id, &t.Name, &t.Locked, &t.URLId, &t.Path, &t.Authors, &t.Intro, &t.Headline, &t.Image, &t.BackgroundColor, &t.PartnerImage, &t.PartnerLink, &t.PartnerText); err != nil {
				return nil, err
			}
			themes = append(themes, t)
		}
		if err := rows.Err(); err != nil {
			return nil, err
		}

		return themes, nil
	}
}

// GetThemesExtended returns a list of Themes including standalone exercices.
func GetThemesExtended() ([]*Theme, error) {
	if themes, err := GetThemes(); err != nil {
		return nil, err
	} else {
		// Append standalone exercices fake-themes
		stdthm := &Theme{
			Name:  "Défis indépendants",
			URLId: "_",
			Path:  "exercices",
		}

		if exercices, err := stdthm.GetExercices(); err == nil && len(exercices) > 0 {
			themes = append(themes, stdthm)
		}

		return themes, nil
	}
}

// GetTheme retrieves a Theme from its identifier.
func GetTheme(id int64) (*Theme, error) {
	t := &Theme{}
	if err := DBQueryRow("SELECT id_theme, name, locked, url_id, path, authors, intro, headline, image, background_color, partner_img, partner_href, partner_text FROM themes WHERE id_theme=?", id).Scan(&t.Id, &t.Name, &t.Locked, &t.URLId, &t.Path, &t.Authors, &t.Intro, &t.Headline, &t.Image, &t.BackgroundColor, &t.PartnerImage, &t.PartnerLink, &t.PartnerText); err != nil {
		return t, err
	}

	return t, nil
}

// GetTheme retrieves a Theme from an given Exercice.
func (e *Exercice) GetTheme() (*Theme, error) {
	t := &Theme{}
	if err := DBQueryRow("SELECT id_theme, name, locked, url_id, path, authors, intro, headline, image, background_color, partner_img, partner_href, partner_text FROM themes WHERE id_theme=?", e.IdTheme).Scan(&t.Id, &t.Name, &t.Locked, &t.URLId, &t.Path, &t.Authors, &t.Intro, &t.Headline, &t.Image, &t.BackgroundColor, &t.PartnerImage, &t.PartnerLink, &t.PartnerText); err != nil {
		return t, err
	}

	return t, nil
}

// GetThemeByName retrieves a Theme from its title
func GetThemeByName(name string) (*Theme, error) {
	t := &Theme{}
	if err := DBQueryRow("SELECT id_theme, name, locked, url_id, path, authors, intro, headline, image, background_color, partner_img, partner_text FROM themes WHERE name=?", name).Scan(&t.Id, &t.Name, &t.Locked, &t.URLId, &t.Path, &t.Authors, &t.Intro, &t.Headline, &t.Image, &t.BackgroundColor, &t.PartnerImage, &t.PartnerText); err != nil {
		return t, err
	}

	return t, nil
}

// GetThemeByPath retrieves a Theme from its dirname
func GetThemeByPath(dirname string) (*Theme, error) {
	t := &Theme{}
	err := DBQueryRow("SELECT id_theme, name, locked, url_id, path, authors, intro, headline, image, background_color, partner_img, partner_href, partner_text FROM themes WHERE path=?", dirname).Scan(&t.Id, &t.Name, &t.Locked, &t.URLId, &t.Path, &t.Authors, &t.Intro, &t.Headline, &t.Image, &t.BackgroundColor, &t.PartnerImage, &t.PartnerLink, &t.PartnerText)

	return t, err
}

// CreateTheme creates and fills a new struct Theme and registers it into the database.
func CreateTheme(theme *Theme) (*Theme, error) {
	if res, err := DBExec("INSERT INTO themes (name, locked, url_id, authors, path, intro, headline, image, background_color, partner_img, partner_href, partner_text) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", theme.Name, theme.Locked, theme.URLId, theme.Authors, theme.Path, theme.Intro, theme.Headline, theme.Image, theme.BackgroundColor, theme.PartnerImage, theme.PartnerLink, theme.PartnerText); err != nil {
		return nil, err
	} else if theme.Id, err = res.LastInsertId(); err != nil {
		return nil, err
	} else {
		return theme, nil
	}
}

// FixURLId generates a valid URLid from the Theme Title.
// It returns true if something has been generated.
func (t *Theme) FixURLId() bool {
	if t.URLId == "" {
		t.URLId = ToURLid(t.Name)
		return true
	}
	return false
}

// Update applies modifications back to the database.
func (t *Theme) Update() (int64, error) {
	if res, err := DBExec("UPDATE themes SET name = ?, locked = ?, url_id = ?, authors = ?, path = ?, intro = ?, headline = ?, image = ?, background_color = ?, partner_img = ?, partner_href = ?, partner_text = ? WHERE id_theme = ?", t.Name, t.Locked, t.URLId, t.Authors, t.Path, t.Intro, t.Headline, t.Image, t.BackgroundColor, t.PartnerImage, t.PartnerLink, t.PartnerText, t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

// Delete the theme from the database.
func (t *Theme) Delete() (int64, error) {
	if res, err := DBExec("DELETE FROM themes WHERE id_theme = ?", t.Id); err != nil {
		return 0, err
	} else if nb, err := res.RowsAffected(); err != nil {
		return 0, err
	} else {
		return nb, err
	}
}

func (t *Theme) deleteFunc(f func(*Exercice) (int64, error)) (int64, error) {
	exercices, err := t.GetExercices()
	if err != nil {
		return 0, err
	}

	for _, exercice := range exercices {
		_, err := f(exercice)
		if err != nil {
			return 0, err
		}
	}

	return 1, nil
}

// DeleteCascade the theme from the database, including inner content but not player content.
func (t *Theme) DeleteCascade() (int64, error) {
	_, err := t.deleteFunc(func(e *Exercice) (int64, error) {
		return e.DeleteCascade()
	})
	if err != nil {
		return 0, err
	}

	return t.Delete()
}

// DeleteCascadePlayer delete player content related to this theme.
func (t *Theme) DeleteCascadePlayer() (int64, error) {
	_, err := t.deleteFunc(func(e *Exercice) (int64, error) {
		return e.DeleteCascadePlayer()
	})
	if err != nil {
		return 0, err
	}

	return 1, nil
}

// DeleteDeep the theme from the database, including player content.
func (t *Theme) DeleteDeep() (int64, error) {
	_, err := t.deleteFunc(func(e *Exercice) (int64, error) {
		return e.DeleteDeep()
	})
	if err != nil {
		return 0, err
	}

	return t.Delete()
}
