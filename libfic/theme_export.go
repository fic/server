package fic

import (
	"fmt"
	"path"
	"strings"
)

var GlobalScoreCoefficient float64 = 1

// exportedExercice is a structure representing a challenge, as exposed to players.
type exportedExercice struct {
	Id              int64    `json:"id"`
	Title           string   `json:"title"`
	Authors         string   `json:"authors,omitempty"`
	Headline        string   `json:"headline,omitempty"`
	Image           string   `json:"image,omitempty"`
	BackgroundColor string   `json:"background_color,omitempty"`
	URLId           string   `json:"urlid"`
	Tags            []string `json:"tags"`
	Gain            int64    `json:"gain"`
	Coeff           float64  `json:"curcoeff"`
	Solved          int64    `json:"solved"`
	Tried           int64    `json:"tried"`
}

// exportedTheme is a structure representing a Theme, as exposed to players.
type exportedTheme struct {
	Name            string             `json:"name,omitempty"`
	URLId           string             `json:"urlid"`
	Locked          bool               `json:"locked,omitempty"`
	Authors         string             `json:"authors,omitempty"`
	Headline        string             `json:"headline,omitempty"`
	Intro           string             `json:"intro,omitempty"`
	Image           string             `json:"image,omitempty"`
	BackgroundColor string             `json:"background_color,omitempty"`
	PartnerImage    string             `json:"partner_img,omitempty"`
	PartnerLink     string             `json:"partner_href,omitempty"`
	PartnerText     string             `json:"partner_txt,omitempty"`
	Exercices       []exportedExercice `json:"exercices"`
}

// Exportedthemes exports themes from the database, to be displayed to players.
func ExportThemes() (interface{}, error) {
	if themes, err := GetThemes(); err != nil {
		return nil, err
	} else {
		// Append standalone exercices fake-themes
		themes = append(themes, &Theme{
			Name:  "Défis indépendants",
			URLId: "_",
			Path:  "exercices",
		})

		ret := map[string]exportedTheme{}
		for _, theme := range themes {
			exos := []exportedExercice{}

			if exercices, err := theme.GetExercices(); err != nil {
				return nil, err
			} else if theme.URLId == "_" && len(exercices) == 0 {
				// If no standalone exercices, don't append them
				continue
			} else {
				for _, exercice := range exercices {
					if exercice.Disabled && theme.Locked {
						continue
					}

					exoimgpath := ""
					if len(exercice.Image) > 0 {
						exoimgpath = path.Join(FilesDir, exercice.Image)
					}
					exobackgroundcolor := ""
					if exercice.BackgroundColor > 0 || len(exercice.Image) > 0 {
						exobackgroundcolor = fmt.Sprintf("#%06X", exercice.BackgroundColor)
					}

					tags, _ := exercice.GetTags()
					exos = append(exos, exportedExercice{
						exercice.Id,
						exercice.Title,
						exercice.Authors,
						exercice.Headline,
						exoimgpath,
						exobackgroundcolor,
						exercice.URLId,
						tags,
						int64(float64(exercice.Gain) * GlobalScoreCoefficient),
						exercice.Coefficient,
						exercice.SolvedCount(),
						exercice.TriedTeamCount(),
					})
				}
			}

			imgpath := ""
			if len(theme.Image) > 0 {
				imgpath = path.Join(FilesDir, theme.Image)
			}

			thmbackgroundcolor := ""
			if theme.BackgroundColor > 0 || len(theme.Image) > 0 {
				thmbackgroundcolor = fmt.Sprintf("#%06X", theme.BackgroundColor)
			}

			partnerImgpath := ""
			if len(theme.PartnerImage) > 0 {
				partnerImgpath = path.Join(FilesDir, theme.PartnerImage)
			}

			ret[fmt.Sprintf("%d", theme.Id)] = exportedTheme{
				theme.Name,
				theme.URLId,
				theme.Locked,
				theme.Authors,
				theme.Headline,
				strings.Replace(theme.Intro, "$FILES$", FilesDir, -1),
				imgpath,
				thmbackgroundcolor,
				partnerImgpath,
				theme.PartnerLink,
				theme.PartnerText,
				exos,
			}
		}

		return ret, nil
	}
}
