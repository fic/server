# NixOS configuration

## Building

```bash
# For backend (Phobos)
nixos-rebuild switch --flake /path/to/flake.nix/directory/#phobos
# For frontend (Deimos)
nixos-rebuild switch --flake /path/to/flake.nix/directory/#deimos
```
