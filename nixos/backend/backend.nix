{ config, lib, pkgs, ... }:
{
  imports = [
    ./db.nix
    ./fic-admin.nix
    ./fic-checker.nix
    ./fic-dashboard.nix
    ./fic-evdist.nix
    ./fic-generator.nix
    ./fic-synchro.nix
  ];

  config.sops = {
    defaultSopsFile = ../secrets/phobos.yml; # We are currently in /nix/store/...-source/backend/
    secrets.phobos_ssh = { mode = "0400"; };
    # You may need to manualy remove `/run/secrets` if modified
  };

  config.system.activationScripts = {
    # Create /var/lib/fic/** directories
    makeFicDirs = lib.stringAfter [ "var" ] ''
      mkdir -p /var/lib/fic/dashboard;
      mkdir -p /var/lib/fic/files;
      mkdir -p /var/lib/fic/pki;
      mkdir -p /var/lib/fic/raw_files;
      mkdir -p /var/lib/fic/settings;
      mkdir -p /var/lib/fic/settingsdist;
      mkdir -p /var/lib/fic/ssh;
      mkdir -p /var/lib/fic/submissions;
      mkdir -p /var/lib/fic/sync;
      mkdir -p /var/lib/fic/teams;
      mkdir -p /var/log/frontend;
    '';
    # Create docker network
    createDockerNetworkPhobos =
      let
        docker = config.virtualisation.oci-containers.backend;
        dockerBin = "${pkgs.${docker}}/bin/${docker}";
      in
      ''
        ${dockerBin} network inspect phobos-lan >/dev/null 2>&1 \
          || ${dockerBin} network create phobos-lan --subnet 172.18.0.0/24
      '';
  };

  config = {
    networking.hostName = "phobos";

    # This is needed to install fic related pkgs
    nixpkgs.config.allowUnfree = true;

    # To switch, remove `phobos-lan` from the networks before running nixos-rebuild
    # ```
    # ${dockerBin} network rm phobos-lan
    # ```
    virtualisation.docker.enable = true;
    virtualisation.podman.enable = false;
    virtualisation.oci-containers.backend = "docker";
  };
}
