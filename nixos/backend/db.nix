{ config, ... }:
{
  config.virtualisation.oci-containers.containers.mariadb = {
    image = "mariadb:latest";
    cmd = [
      "/bin/bash"
      "/usr/local/bin/docker-entrypoint.sh"
      "mysqld"
    ];
    ports = [ "3306:3306" ];
    extraOptions = [ "--network=phobos-lan" "--ip=172.18.0.42" ];
    environment = {
      MYSQL_DATABASE = "fic";
      MYSQL_USER = "fic";
      MYSQL_PASSWORD = "fic";
      MYSQL_RANDOM_ROOT_PASSWORD = "yes";
    };
    volumes = [
      "/etc/hosts:/etc/hosts:ro"
      "/etc/mysql/conf.d:/etc/mysql/conf.d:ro"
      "/var/lib/fic/mysql:/var/lib/mysql"
    ];
  };
}
