{ config, inputs, pkgs, ... }:
{
  config.virtualisation.oci-containers.containers.fic-admin = {
    image = "fic-admin:latest";
    imageFile = pkgs.dockerTools.buildImage {
      name = "fic-admin";
      tag = "latest";
      created = "now";
      config = {
        Cmd = [ "${inputs.ficpkgs.packages.x86_64-linux.fic-admin}/bin/admin" ];
      };
    };
    autoStart = true;
    cmd = [
      "${inputs.ficpkgs.packages.x86_64-linux.fic-admin}/bin/admin"
      "-4real"
      "-bind=0.0.0.0:8081"
      "-baseurl=/admin/"
      "-localimport=/mnt/fic"
      "-timestampCheck=/srv/submissions"
    ];
    ports = [ "8081:8081" ];
    extraOptions = [ "--network=phobos-lan" "--ip=172.18.0.40" ];
    environment = {
      MYSQL_HOST = "db";
      FICCA_PASS = "jee8AhloAith1aesCeQu5ahgIegaeM4K";
    };
    volumes = [
      "/etc/hosts:/etc/hosts:ro"
      "/var/lib/fic/generator:/srv/GENERATOR:ro"
      "/var/lib/fic/raw_files:/mnt/fic"
      "/var/lib/fic/dashboard:/srv/DASHBOARD"
      "/var/lib/fic/files:/srv/FILES"
      "/var/lib/fic/pki:/srv/PKI"
      "/var/lib/fic/teams:/srv/TEAMS:ro"
      "/var/lib/fic/settings:/srv/SETTINGS"
      "/var/lib/fic/submissions:/srv/submissions:ro"
    ];
  };
}
