{ config, inputs, pkgs, ... }:
{
  config.virtualisation.oci-containers.containers.fic-checker = {
    image = "fic-checker:latest";
    imageFile = pkgs.dockerTools.buildImage {
      name = "fic-checker";
      tag = "latest";
      created = "now";
      config = {
        Cmd = [ "${inputs.ficpkgs.packages.x86_64-linux.fic-checker}/bin/checker" ];
      };
    };
    autoStart = true;
    environment = {
      MYSQL_HOST = "db";
    };
    workdir = "/srv";
    extraOptions = [ "--network=phobos-lan" "--ip=172.18.0.41" ];
    volumes = [
      "/etc/hosts:/etc/hosts:ro"
      "/var/lib/fic/generator:/srv/GENERATOR:ro"
      "/var/lib/fic/teams:/srv/TEAMS:ro"
      "/var/lib/fic/settingsdist:/srv/SETTINGSDIST:ro"
      "/var/lib/fic/submissions:/srv/submissions"
    ];
  };
}
