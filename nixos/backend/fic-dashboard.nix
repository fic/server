{ config, inputs, pkgs, ... }:
{
  config.virtualisation.oci-containers.containers.fic-dashboard = {
    image = "fic-dashboard:latest";
    imageFile = pkgs.dockerTools.buildImage {
      name = "fic-dashboard";
      tag = "latest";
      created = "now";
      config = {
        Cmd = [ "${inputs.ficpkgs.packages.x86_64-linux.fic-dashboard}/bin/dashboard" ];
      };
    };
    autoStart = true;
    cmd = [
      "${inputs.ficpkgs.packages.x86_64-linux.fic-dashboard}/bin/dashboard"
      "-bind=:8082"
      "-restrict-to-ips=/srv/DASHBOARD/restricted-ips.json"
    ];
    ports = [ "8082:8082" ];
    volumes = [
      "/etc/hosts:/etc/hosts:ro"
      "/var/lib/fic/dashboard:/srv/DASHBOARD:ro"
      "/var/lib/fic/files:/srv/FILES:ro"
      "/var/lib/fic/teams:/srv/TEAMS:ro"
      "/var/lib/fic/settingsdist:/srv/SETTINGSDIST:ro"
    ];
  };
}
