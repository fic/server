{ config, inputs, pkgs, ... }:
{
  config.virtualisation.oci-containers.containers.fic-evdist = {
    image = "fic-evdist:latest";
    imageFile = pkgs.dockerTools.buildImage {
      name = "fic-evdist";
      tag = "latest";
      created = "now";
      config = {
        Cmd = [ "${inputs.ficpkgs.packages.x86_64-linux.fic-evdist}/bin/evdist" ];
      };
    };
    autoStart = true;
    workdir = "/srv";
    volumes = [
      "/etc/hosts:/etc/hosts:ro"
      "/var/lib/fic/settings:/srv/SETTINGS"
      "/var/lib/fic/settingsdist:/srv/SETTINGSDIST"
    ];
  };
}
