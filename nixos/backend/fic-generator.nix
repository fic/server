{ config, inputs, pkgs, ... }:
{
  config.virtualisation.oci-containers.containers.fic-generator = {
    image = "fic-generator:latest";
    imageFile = pkgs.dockerTools.buildImage {
      name = "fic-generator";
      tag = "latest";
      created = "now";
      config = {
        Cmd = [ "${inputs.ficpkgs.packages.x86_64-linux.fic-generator}/bin/generator" ];
      };
    };
    autoStart = true;
    environment = {
      MYSQL_HOST = "db";
    };
    workdir = "/srv";
    extraOptions = [ "--network=phobos-lan" "--ip=172.18.0.41" ];
    volumes = [
      "/etc/hosts:/etc/hosts:ro"
      "/var/lib/fic/generator:/srv/GENERATOR"
      "/var/lib/fic/teams:/srv/TEAMS"
      "/var/lib/fic/settingsdist:/srv/SETTINGSDIST:ro"
    ];
  };
}
