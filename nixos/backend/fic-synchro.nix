{ config, inputs, pkgs, ... }:
{
  config.virtualisation.oci-containers.containers.fic-synchro =
    {
      image = "fic-synchro:latest";
      imageFile = pkgs.dockerTools.buildImage {
        name = "fic-synchro";
        tag = "latest";
        created = "now";
        copyToRoot = pkgs.buildEnv {
          name = "packagelist";
          paths = [ pkgs.coreutils pkgs.openssh pkgs.rsync ];
        };
        config = {
          Cmd = [ "${inputs.ficpkgs.packages.x86_64-linux.fic-synchro}/bin/synchro" ];
        };
        runAsRoot = ''
          #!${pkgs.runtimeShell}
          ${pkgs.dockerTools.shadowSetup}
          mkdir -p /tmp/
          chmod a+rwx /tmp/
        '';
      };
      autoStart = true;
      extraOptions = [ "--network=phobos-lan" "--ip=172.18.0.43" ];
      volumes = [
        "/etc/hosts:/etc/hosts:ro"
        "/var/lib/fic/ssh:/etc/ssh:ro"
        "${config.sops.secrets.phobos_ssh.path}:/root/.ssh/id_ed25519:ro"
        "/var/lib/fic/files:/srv/FILES:ro"
        #"/var/lib/fic/pki/ca.key:/srv/PKI/ca.key:ro"
        "/var/lib/fic/pki/shared:/srv/PKI/shared:ro"
        "/var/lib/fic/settingsdist:/srv/SETTINGSDIST:ro"
        "/var/lib/fic/submissions:/srv/submissions"
        "/var/lib/fic/teams:/srv/TEAMS:ro"
        "/var/log/frontend:/var/log/frontend"
      ];
    };
}
