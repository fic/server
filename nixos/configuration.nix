{ config, pkgs, ... }:
{
  imports = [
    ./hardware-configuration.nix
    ./locale.nix
    ./network.nix
    ./packages.nix
    ./registry.nix
    ./users.nix
  ] ++ (if (import ../config-var.nix).efi then [ ./efi.nix ] else [ ./bios.nix ]);

  system.stateVersion = "22.05";
}
