{
  description = "Fic Servers Nix Configuration";

  inputs = {
    nixpkgs = { url = "github:nixos/nixpkgs/nixos-unstable"; };
    ficpkgs = {
      # Vendor hash of fic-server's flake.nix must be up to date
      #url = "git+https://git.nemunai.re/fic/server";
      # For local testing only
      url = "/root/fic-server";

      inputs.nixpkgs.follows = "nixpkgs";
    };
    sops-nix = {
      url = "github:thouveninantoine/sops-nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = inputs: {
    nixosConfigurations =
      let
        common_modules = [
          ./configuration.nix
          inputs.sops-nix.nixosModules.sops
          "${inputs.nixpkgs}/nixos/modules/installer/cd-dvd/installation-cd-base.nix"
        ];
      in
      {
        phobos = inputs.nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
            ./backend/backend.nix
          ] ++ common_modules;
          specialArgs = { inherit inputs; };
        };
        deimos = inputs.nixpkgs.lib.nixosSystem {
          system = "x86_64-linux";
          modules = [
          ] ++ common_modules;
          specialArgs = { inherit inputs; };
        };
      };
  };
}
