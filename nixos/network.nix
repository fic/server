{ ... }:
{
  networking.useDHCP = false;
  networking.interfaces.eno1.useDHCP = true;
  networking.interfaces.enp1s0.useDHCP = true;

  networking.extraHosts = ''
    ${(import ./config-var.nix).ip.phobos} phobos

    172.18.0.40     admin
    172.18.0.41     backend
    172.18.0.42     db
    172.18.0.43     synchro

    ${(import ./config-var.nix).ip.deimos} deimos

    172.18.1.2      nginx
    172.18.1.3      frontend
    172.18.1.4      auth

    127.0.0.1       localhost
    ::1     localhost ip6-localhost ip6-loopback
    fe00::0 ip6-localnet
    ff00::0 ip6-mcastprefix
    ff02::1 ip6-allnodes
    ff02::2 ip6-allrouters
  '';

  services.openssh = {
    enable = true;
    passwordAuthentication = false;
    listenAddresses = [
      { addr = "0.0.0.0"; port = 2222; }
    ];
  };

  networking.firewall.allowedTCPPorts = [ 22 2222 ];

  systemd.services.sshd.after = [ "network-interfaces.target" ];
}
