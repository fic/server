{ ... }:
{
  users = {
    mutableUsers = false;
    users.fic = {
      isNormalUser = true;
      extraGroups = [ "wheel" ];
      openssh.authorizedKeys.keys = [
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAILBoJRKGvhpJGYQfq+Ocp83nJixk8zz3cmzHOvLIW2C9 antoine.thouvenin"
        "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJdjG/+FTghcl+sgsAFM7kdBTbGIR9JycgpWeLGJt2ZV elie.brami"
      ];
      hashedPassword = "$6$CuDkmaet$ZWh.KlzZe2EF2c23GErwdbsa1naByrNe15j7Jy3SuJZfEwGUV16QEkz9bcfzHtMteTjGRr8ixOtKYn.wV8e10.";
    };
  };
}
