Password Paper
==============

These scripts generate A4 sheets filled with 2x5


## Requirements

Those scripts requires XeLaTeX and the `texlive-fonts-extra` package.

It uses the TexLive
[dingbat package](http://www.ctan.org/tex-archive/fonts/dingbat/).

The fonts used come from the
[Libertine open fonts project](http://linuxlibertine.org/) and
[Fantasque Sans Mono](https://github.com/belluzj/fantasque-sans).


## Usage

First, you need to generate the file containing teams and passwords.


### Password file format

Passwords are given through a DSV file, using `:` as separator.

For example, a valid `teams.pass` file with 6 teams could be:

```
Abricot:hie9iJ<iexue
Cyan:ooxae^L4aeCh
Vert:ahNgei3Yoo@w
Moutarde:gua3FaiThi]a
Zinzolin:eep7Ac}eeH3a
Safre:di4eeH\ae9io
```


### Generate the PDF

Just run `make` to generate the PDf.
