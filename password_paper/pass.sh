#!/bin/sh

cat <<EOF
\\documentclass[a4paper]{article}

\\usepackage[frenchb]{babel}
\\usepackage[margin=0.6cm,inner=0cm,outer=0cm]{geometry}
\\usepackage{fontspec}
\\usepackage{hyperref}
\\usepackage{xcolor}
\\usepackage{graphicx}

\\definecolor{grey}{rgb}{0.15, 0.15, 0.15}
\\hypersetup{
    colorlinks  = true,
    urlcolor    = grey,
    linkcolor   = grey,
}

\\setmainfont[Ligatures=TeX,Numbers={OldStyle,Proportional}]{Linux Libertine O}
\\setsansfont[Ligatures=TeX,Numbers={OldStyle,Proportional}]{Linux Biolinum O}
\\setmonofont{FantasqueSansMono-Regular.otf}

\\newcommand{\\zz}[3]{\\begin{minipage}{.48\\textwidth}
    \\vspace{1.8em}
    \\centering
        \\begin{minipage}{.11\\textwidth}
          \\hspace*{0.2cm}
          \\includegraphics[width=1.5\\linewidth]{epita}
        \\end{minipage}
        \\hfill
        \\begin{minipage}{.6\\textwidth}
          \\centering
          {\\sffamily\\huge Bienvenue au challenge forensic~!}
        \\end{minipage}
        \\hfill
        \\begin{minipage}{.11\\textwidth}
          \\hspace*{-0.7cm}
          \\includegraphics[width=1\\linewidth]{fic}
        \\end{minipage}
        \\par
        \\vspace{.7em}
        \\rule{0.58\\textwidth}{1.5pt}
        \\vspace{1.2em}

        Rendez-vous sur \\url{https://fic.srs.epita.fr/}.

        \\vspace{0.1em}
        \\begin{minipage}{.11\\textwidth}
          \\vspace{2em}
          \\hspace*{0.5cm}
          \\includegraphics[width=1.5\\linewidth]{srs}
        \\end{minipage}
        \\hfill
        \\begin{minipage}{.7\\textwidth}
          \\centering
          Votre équipe : #1 -- \\emph{#2}\\\\
          Mot de passe : \\texttt{#3}\\\\
          \\vspace{1em}
          Bon courage~!
        \\end{minipage}
        \\hfill
        \\begin{minipage}{.11\\textwidth}
          \\vspace{2em}
          \\hspace*{-1cm}
          \\includegraphics[width=1.5\\linewidth]{dnred}
        \\end{minipage}
        \\vspace{0.5em}
  \\end{minipage}
}

\\begin{document}
EOF


ID=0
while read LINE
do
    ID=$(($ID + 1))
    NAME=$(echo $LINE | cut -d : -f 1 | sed 's/&/\\&/g;s/%/\\%/g;s/\$/\\$/g;s/#/\\#/g;s/_/ /g;s/{/\\{/g;s/}/\\}/g;s/~/\\textasciitilde{}/g;s/\^/\\\^{}/g')
    PASS=$(echo $LINE | cut -d : -f 2- | sed 's/&/\\&/g;s/%/\\%/g;s/\$/\\$/g;s/#/\\#/g;s/_/\\_/g;s/{/\\{/g;s/}/\\}/g;s/~/\\textasciitilde{}/g;s/\^/\\\^{}/g')

    echo "\\zz{$ID}{$NAME}{$PASS}"

    if [ $(($ID % 2)) = 0 ]
    then
	echo
    fi
done


cat <<EOF
\\end{document}
EOF
