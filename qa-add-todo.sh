#!/bin/bash

source qa-common.sh

TEAMNUM=$1
THEMETODO=$2
MIN_EXO_TODO=$3
[ $# -gt 3 ] && MAX_EXO_TODO=$4 || MAX_EXO_TODO=$MIN_EXO_TODO
MIN_EXO_TODO=$(($MIN_EXO_TODO - 1))

curl -s http://${HOST_FICADMIN}/api/teams | jq -r '.[] | [(.id | tostring), " ", .name] | add' | grep "${TEAM_PREFIX} ${TEAMNUM}" | while read TEAMID TEAMSTR
do
	if echo $TEAMSTR | grep "${TEAM_PREFIX}" > /dev/null 2> /dev/null
	then
		GRPFIC=$(echo $TEAMSTR | sed -r "s/${TEAM_PREFIX} //")

		curl -s http://${HOST_FICADMIN}/api/themes.json | jq -r '.["'$THEMETODO'"] | .exercices['$MIN_EXO_TODO:$MAX_EXO_TODO'] | .[].id' | while read EXID
		do
			curl -X POST -d @- -H "X-FIC-Team: ${QA_ADMIN}" http://${HOST_FICQA}/api/qa_work.json <<EOF
{"id_team": $TEAMID, "id_exercice": $EXID}
EOF
		done
	fi
done
