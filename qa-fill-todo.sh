#!/bin/bash

source qa-common.sh

MIN_EXO_TODO=$(($MIN_LVL_TODO - 1))
MAX_EXO_TODO=$(($MAX_LVL_TODO))

curl -s http://${HOST_FICADMIN}/api/teams | jq -r '.[] | [(.id | tostring), " ", .name] | add' | while read TEAMID TEAMSTR
do
	if echo $TEAMSTR | grep "${TEAM_PREFIX}" > /dev/null 2> /dev/null
	then
		GRPFIC=$(echo $TEAMSTR | sed -r "s/${TEAM_PREFIX} //")

		THEMES_TO_TESTS=$(curl -s http://${HOST_FICADMIN}/api/themes | jq -r '.[] | [(.id | tostring), " ", (.path | split("-") | .[0])] | add' | grep -v " grp$GRPFIC" | shuf | head -n $NB_THEMES_TODO | cut -d " " -f 1 | xargs | sed 's/^/"/;s/ /","/g;s/$/"/')

		curl -s http://${HOST_FICADMIN}/api/themes.json | jq -r '.['$THEMES_TO_TESTS'] | .exercices['$MIN_EXO_TODO:$MAX_EXO_TODO'] | .[].id' | while read EXID
		do
		    curl -X POST -d @- -H "X-FIC-Team: ${QA_ADMIN}" http://${HOST_FICQA}/api/qa_work.json <<EOF
{"id_team": $TEAMID, "id_exercice": $EXID}
EOF
		done
	fi
done
