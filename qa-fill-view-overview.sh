#!/bin/bash

source qa-common.sh

TEAM_OVERVIEW=$1

# Register Teams association betweeen "FIC Groupe X" and X, the theme directory prefix
`curl -s http://${HOST_FICADMIN}/api/teams | jq -r '.[] | [(.id | tostring), " ", .name] | add' | while read ID TEAM; do if echo $TEAM | grep "${TEAM_PREFIX}" > /dev/null 2> /dev/null; then echo export TEAM_$TEAM=$ID | sed -r "s/${TEAM_PREFIX} //"; fi; done`

# Add all themes and exercices to assistants
curl -s http://${HOST_FICADMIN}/api/themes | jq '.[].id' | while read tid
do
	TEAM="TEAM_$(curl -s http://${HOST_FICADMIN}/api/themes/$tid | jq -r .path | sed -r 's/^[^0-9]*([0-9]+)-.*$/\1/')"
	curl -s http://${HOST_FICADMIN}/api/themes/$tid/exercices | jq .[].id | while read ex
	do
		curl -X POST -d @- -H "X-FIC-Team: ${QA_ADMIN}" http://${HOST_FICQA}/api/qa_my_exercices.json <<EOF
{"id_team": ${TEAM_OVERVIEW}, "id_exercice": $ex}
EOF
	done
done
