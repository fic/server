package api

import (
	"encoding/json"
	"flag"
	"io"
	"net/http"
	"net/url"
	"os"
	"path"
)

var adminLink string

func init() {
	flag.StringVar(&adminLink, "admin-link", os.Getenv("FIC_QA_ADMIN_LINK"), "URL to admin interface, to use its features as replacement")
}

func fwdAdmin(method, urlpath string, body io.Reader, out interface{}) error {
	u, err := url.Parse(adminLink)
	if err != nil {
		return err
	}

	var user, pass string
	if u.User != nil {
		user = u.User.Username()
		pass, _ = u.User.Password()
		u.User = nil
	}

	u.Path = path.Join(u.Path, urlpath)

	r, err := http.NewRequest(method, u.String(), body)
	if err != nil {
		return err
	}

	if len(user) != 0 || len(pass) != 0 {
		r.SetBasicAuth(user, pass)
	}

	resp, err := http.DefaultClient.Do(r)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	dec := json.NewDecoder(resp.Body)
	return dec.Decode(&out)
}
