package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func DeclareRoutes(router *gin.RouterGroup) {
	apiRoutes := router.Group("/api")
	apiRoutes.Use(authMiddleware())

	declareExercicesRoutes(apiRoutes)
	declareQARoutes(apiRoutes)
	declareThemesRoutes(apiRoutes)
	declareTodoRoutes(apiRoutes)
	declareVersionRoutes(apiRoutes)
	declareGitlabRoutes(router, apiRoutes)

	apiManagerRoutes := router.Group("/api")
	apiManagerRoutes.Use(authMiddleware(func(ficteam string, teamid int64, c *gin.Context) bool {
		for _, manager := range ManagerUsers {
			if manager == ficteam {
				return true
			}
		}

		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"errmsg": "Not authorized."})
		return false
	}))

	declareTodoManagerRoutes(apiManagerRoutes)
	declareTeamsRoutes(apiManagerRoutes)
}
