package api

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	"srs.epita.fr/fic-server/libfic"

	"github.com/gin-gonic/gin"
)

func declareTeamsRoutes(router *gin.RouterGroup) {
	router.GET("/teams", listTeams)

	teamsRoutes := router.Group("/teams/:tid")
	teamsRoutes.Use(teamHandler)
	teamsRoutes.GET("", showTeam)

	declareTodoRoutes(teamsRoutes)
	declareTodoManagerRoutes(teamsRoutes)
}

func teamHandler(c *gin.Context) {
	var team *fic.Team
	if tid, err := strconv.ParseInt(string(c.Param("tid")), 10, 64); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"errmsg": "Bad team identifier."})
		return
	} else if team, err = fic.GetTeam(tid); err != nil {
		c.AbortWithStatusJSON(http.StatusNotFound, gin.H{"errmsg": "Team not found."})
		return
	}

	c.Set("team", team)

	c.Next()
}

func listTeams(c *gin.Context) {
	teams, err := fic.GetTeams()
	if err != nil {
		log.Println("Unable to GetTeams: ", err.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"errmsg": fmt.Sprintf("Unable to list teams: %s", err.Error())})
		return
	}

	c.JSON(http.StatusOK, teams)
}

func showTeam(c *gin.Context) {
	c.JSON(http.StatusOK, c.MustGet("team"))
}
