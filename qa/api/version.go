package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

func declareVersionRoutes(router *gin.RouterGroup) {
	router.GET("/version", showVersion)
}

func showVersion(c *gin.Context) {
	teamid := c.MustGet("LoggedTeam").(int64)
	ficteam := c.MustGet("LoggedUser").(string)

	var ismanager bool

	for _, manager := range ManagerUsers {
		if manager == ficteam {
			ismanager = true
			break
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"version": 0.4,
		"auth": map[string]interface{}{
			"name":       ficteam,
			"id_team":    teamid,
			"is_manager": ismanager,
		},
	})
}
