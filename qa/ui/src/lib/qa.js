export const QAStates = {
  "ok": "OK",
  "timer": "Temps passé",
  "orthograph": "Orthographe et grammaire",
  "issue-statement": "Pas compris",
  "issue-flag": "Problème de flag",
  "issue-mcq": "Problème de QCM/QCU",
  "issue-hint": "Problème d'indice",
  "issue-file": "Problème de fichier",
  "issue": "Problème autre",
  "suggest": "Suggestion",
  "too-hard": "Trop dur",
  "too-easy": "Trop facile",
};

export class QAQuery {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_exercice, id_team, user, creation, state, subject, solved, closed, exported, forge_link }) {
    this.id = id;
    this.id_team = id_team;
    this.id_exercice = id_exercice;
    this.user = user;
    this.creation = creation;
    this.state = state;
    this.subject = subject;
    this.solved = solved;
    this.closed = closed;
    this.exported = exported;
    this.forge_link = forge_link;
  }

  async delete() {
    const res = await fetch(`api/exercices/${this.id_exercice}/qa/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status < 300) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save() {
    const res = await fetch(this.id?`api/exercices/${this.id_exercice}/qa/${this.id}`:`api/exercices/${this.id_exercice}/qa`, {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async export2Gitlab() {
    const res = await fetch(`api/qa/${this.id}/gitlab_export`, {method: 'POST', headers: {'Accept': 'application/json'}})
    if (res.status == 200) {
      return await res.json();
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getExerciceQA(eid) {
  const res = await fetch(`api/exercices/${eid}/qa`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data == null)
      return [];
    return data.map((t) => new QAQuery(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export class QAComment {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_team, user, date, content }) {
    this.id = id;
    this.id_team = id_team;
    this.user = user;
    this.date = date;
    this.content = content;
  }

  async delete(qid) {
    const res = await fetch(`api/qa/${qid}/comments/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status < 300) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }

  async save(qid) {
    const res = await fetch(this.id?`api/qa/${qid}/comments/${this.id}`:`api/qa/${qid}/comments`, {
      method: this.id?'PUT':'POST',
      headers: {'Accept': 'application/json'},
      body: JSON.stringify(this),
    });
    if (res.status == 200) {
      const data = await res.json();
      this.update(data);
      return data;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getQAComments(qid) {
  const res = await fetch(`api/qa/${qid}/comments`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data == null)
      return [];
    return data.map((t) => new QAComment(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
