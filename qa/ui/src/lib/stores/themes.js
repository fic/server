import { writable, derived } from 'svelte/store';

import { getThemes } from '$lib/themes'

function createThemesStore() {
  const { subscribe, set, update } = writable([]);

  return {
    subscribe,

    set: (v) => {
      update((m) => Object.assign(m, v));
    },

    update,

    refresh: async () => {
      const list = await getThemes();
      update((m) => list);
      return list;
    },
  };

}

export const themes = createThemesStore();

export const themesIdx = derived(
  themes,
  $themes => {
    const themes_idx = { };

    for (const t of $themes) {
      themes_idx[t.id] = t;
    }

    return themes_idx;
  },
);
