import { createTodosStore } from '$lib/stores/todo.js'

export const fieldsTeams = ["name", "color", "active", "external_id"];

export class Team {
  constructor(res) {
    if (res) {
      this.update(res);
    }

    this.todos = createTodosStore(this);
  }

  update({ id, name, color, active, external_id }) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.active = active;
    this.external_id = external_id;
  }

  toHexColor() {
    let num = this.color;
    num >>>= 0;
    let b = (num & 0xFF).toString(16),
	g = ((num & 0xFF00) >>> 8).toString(16),
	r = ((num & 0xFF0000) >>> 16).toString(16),
	a = ( (num & 0xFF000000) >>> 24 ) / 255 ;
    if (r.length <= 1) r = "0" + r;
    if (g.length <= 1) g = "0" + g;
    if (b.length <= 1) b = "0" + b;
    return "#" + r +  g +  b;
  }
}

export async function getTeams() {
  const res = await fetch(`api/teams`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return (await res.json()).map((t) => new Team(t));
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getTeam(tid) {
  const res = await fetch(`api/teams/${tid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Team(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
