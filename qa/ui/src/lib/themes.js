export class Theme {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, name, urlid, path, authors, intro, headline, image, partner_img, partner_href, partner_txt }) {
    this.id = id;
    this.name = name;
    this.urlid = urlid;
    this.path = path;
    this.authors = authors;
    this.intro = intro;
    this.headline = headline;
    this.image = image;
    this.partner_img = partner_img;
    this.partner_href = partner_href;
    this.partner_txt = partner_txt;
  }
}

export async function getThemes() {
  const res = await fetch(`api/themes`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data) {
      return data.map((t) => new Theme(t));
    } else {
      return [];
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getTheme(tid) {
  const res = await fetch(`api/themes/${tid}`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return new Theme(await res.json());
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
