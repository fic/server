import { QAQuery } from './qa';

export class QATodo {
  constructor(res) {
    if (res) {
      this.update(res);
    }
  }

  update({ id, id_team, id_exercice }) {
    this.id = id;
    this.id_team = id_team;
    this.id_exercice = id_exercice;
  }

  async delete(team) {
    const res = await fetch(team?`api/teams/${team.id}/todo/${this.id}`:`api/teams/${this.id_team}/todo/${this.id}`, {
      method: 'DELETE',
      headers: {'Accept': 'application/json'}
    });
    if (res.status < 300) {
      return true;
    } else {
      throw new Error((await res.json()).errmsg);
    }
  }
}

export async function getQATodo(team) {
  const res = await fetch(team?`api/teams/${team.id}/qa_work.json`:`api/qa_work.json`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json();
    if (data === null) {
      return []
    } else {
      return data.map((t) => new QATodo(t));
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getQAWork(team) {
  const res = await fetch(team?`api/teams/${team.id}/qa_mywork.json`:`api/qa_mywork.json`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json()
    if (data) {
      return data.map((t) => new QAQuery(t));
    } else {
      return [];
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getQAView(team) {
  const res = await fetch(team?`api/teams/${team.id}/qa_myexercices.json`:`api/qa_myexercices.json`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    const data = await res.json()
    if (data) {
      return data.map((t) => new QATodo(t));
    } else {
      return [];
    }
  } else {
    throw new Error((await res.json()).errmsg);
  }
}

export async function getExerciceTested(team) {
  const res = await fetch(team?`api/teams/${team.id}/qa_exercices.json`:`api/qa_exercices.json`, {headers: {'Accept': 'application/json'}})
  if (res.status == 200) {
    return await res.json();
  } else {
    throw new Error((await res.json()).errmsg);
  }
}
