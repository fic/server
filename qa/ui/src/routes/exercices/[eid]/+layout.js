import { getExercice } from '$lib/exercices';
import { getExerciceQA } from '$lib/qa.js';

/** @type {import('./$types').PageLoad} */
export async function load({ depends, params }) {
  const [exercice, qaitems] = await Promise.all([
    getExercice(params.eid),
    getExerciceQA(params.eid),
  ]);
  depends(`api/exercices/${params.eid}/qa`);
  depends(`api/exercices/${params.eid}`);

  return { exercice, qaitems };
}
