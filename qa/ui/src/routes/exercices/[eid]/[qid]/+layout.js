import { error } from '@sveltejs/kit';

/** @type {import('./$types').PageLoad} */
export async function load({ params, parent }) {
  const { exercice, qaitems } = await parent();

  let query_selected = null;
  for (const qaitem of qaitems) {
    if (qaitem.id == params.qid) {
      query_selected = qaitem;
    }
  }

  if (!query_selected) {
    error(404, {
            message: 'Not found'
          });
  }

  return { exercice, qaitems, query_selected };
}
