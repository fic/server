import { getTheme } from '$lib/themes';

/** @type {import('./$types').PageLoad} */
export async function load({ depends, params }) {
  const theme = await getTheme(params.tid)
  depends(`api/themes/${params.tid}`);

  return { theme };
}
