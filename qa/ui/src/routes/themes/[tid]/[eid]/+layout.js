import { getExercice } from '$lib/exercices';
import { getExerciceQA } from '$lib/qa.js';

/** @type {import('./$types').PageLoad} */
export async function load({ depends, params, parent }) {
  const { theme } = await parent();

  const [exercice, qaitems] = await Promise.all([
    getExercice(params.eid),
    getExerciceQA(params.eid),
  ]);
  depends(`api/exercices/${params.eid}`);
  depends(`api/exercices/${params.eid}/qa`);

  return { exercice, qaitems, theme };
}
