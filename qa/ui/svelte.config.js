import adapt from '@sveltejs/adapter-static';

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapt({
			fallback: 'index.html'
		}),
                paths: {
                        relative: true
                },
	}
};

export default config;
