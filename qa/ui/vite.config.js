import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
        server: {
                hmr: {
                        port: 10001
                }
        },

	plugins: [sveltekit()]
};

export default config;
