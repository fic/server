package main

import (
	"log"
	"net/http"
	"path"
)

var denyNameChange bool = true

func ChNameHandler(w http.ResponseWriter, r *http.Request, team string, sURL []string) {
	if denyNameChange {
		log.Printf("UNHANDELED %s name change request from %s: %s [%s]\n", r.Method, r.RemoteAddr, r.URL.Path, r.UserAgent())
		http.Error(w, "{\"errmsg\":\"Le changement de nom est prohibé.\"}", http.StatusForbidden)
	} else if saveTeamFile(path.Join(team, "name"), w, r) {
		// File enqueued for backend treatment
		http.Error(w, "{\"errmsg\":\"Demande de changement de nom acceptée\"}", http.StatusAccepted)
	}
}
