package main

import (
	"net/http"
	"path"
	"time"
)

func WantChoicesHandler(w http.ResponseWriter, r *http.Request, team string, sURL []string) {
	if challengeEnd != nil && time.Now().After(*challengeEnd) {
		http.Error(w, "{\"errmsg\":\"Le challenge est terminé, trop tard !\"}", http.StatusGone)
		return
	}

	// Enqueue file for backend treatment
	if saveTeamFile(path.Join(team, "choices"), w, r) {
		http.Error(w, "{\"errmsg\":\"Demande de choix acceptée...\"}", http.StatusAccepted)
	}
}
