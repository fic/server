package main

import (
	"net/http"
	"path"
	"time"
)

func HintHandler(w http.ResponseWriter, r *http.Request, team string, sURL []string) {
	if challengeEnd != nil && time.Now().After(*challengeEnd) {
		http.Error(w, "{\"errmsg\":\"Le challenge est terminé, trop tard pour un indice !\"}", http.StatusGone)
		return
	}

	// Enqueue file for backend treatment
	if saveTeamFile(path.Join(team, "hint"), w, r) {
		http.Error(w, "{\"errmsg\":\"Demande d'astuce acceptée...\"}", http.StatusAccepted)
	}
}
