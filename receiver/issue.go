package main

import (
	"log"
	"net/http"
	"path"
)

var acceptNewIssues bool = true

func IssueHandler(w http.ResponseWriter, r *http.Request, team string, sURL []string) {
	if !acceptNewIssues {
		log.Printf("UNHANDELED %s issue request from %s: %s [%s]\n", r.Method, r.RemoteAddr, r.URL.Path, r.UserAgent())
		http.Error(w, "{\"errmsg\":\"Il n'est pas possible de rapporter d'anomalie.\"}", http.StatusForbidden)
	} else if saveTeamFile(path.Join(team, "issue"), w, r) {
		// File enqueued for backend treatment
		http.Error(w, "{\"errmsg\":\"Anomalie signalée avec succès. Merci de votre patience...\"}", http.StatusAccepted)
	}
}
