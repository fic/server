package main

import (
	"net/http"
	"path"
	"time"
)

var enableResetProgression = false

func ResetProgressHandler(w http.ResponseWriter, r *http.Request, team string, sURL []string) {
	if !enableResetProgression {
		http.Error(w, "{\"errmsg\":\"Le challenge est terminé, trop tard !\"}", http.StatusForbidden)
		return
	}

	if challengeEnd != nil && time.Now().After(*challengeEnd) {
		http.Error(w, "{\"errmsg\":\"Le challenge est terminé, trop tard !\"}", http.StatusGone)
		return
	}

	// Enqueue file for backend treatment
	if saveTeamFile(path.Join(team, "reset_progress"), w, r) {
		http.Error(w, "{\"errmsg\":\"Demande acceptée...\"}", http.StatusAccepted)
	}
}
