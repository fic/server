package main

import (
	"log"
	"net/http"
	"net/url"
	"path"
	"strings"
	"text/template"
)

var enableResolutionRoute bool = false

type ResolutionHandler struct{}

const resolutiontpl = `<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Résolution</title>
  </head>
  <body style="margin: 0">
    <video src="{{.}}" controls width="100%" height="100%"></video>
  </body>
</html>
`

func (s ResolutionHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if addr := r.Header.Get("X-Forwarded-For"); addr != "" {
		r.RemoteAddr = addr
	}

	if !enableResolutionRoute {
		log.Printf("UNHANDELED %s request from %s: /resolution%s [%s]\n", r.Method, r.RemoteAddr, r.URL.Path, r.UserAgent())
		http.NotFound(w, r)
		return
	}

	log.Printf("%s \"%s /resolution%s\" [%s]\n", r.RemoteAddr, r.Method, r.URL.Path, r.UserAgent())

	w.Header().Set("Content-Type", "text/html")

	if resolutionTmpl, err := template.New("resolution").Parse(resolutiontpl); err != nil {
		log.Println("Cannot create template: ", err)
	} else if err = resolutionTmpl.Execute(w, path.Join("/vids/", strings.Replace(url.PathEscape(r.URL.Path), "%2F", "/", -1))); err != nil {
		log.Println("An error occurs during template execution: ", err)
	}
}
