package main

import (
	"log"
	"os"
	"time"

	"srs.epita.fr/fic-server/settings"
)

var startedFile = "started"

var touchTimer *time.Timer = nil
var challengeStart time.Time
var challengeEnd *time.Time

func touchStartedFile() {
	if fd, err := os.Create(startedFile); err == nil {
		log.Println("Started! Go, Go, Go!!")
		fd.Close()
	} else {
		log.Fatal("Unable to start challenge:", err)
	}
}

func reloadSettings(config *settings.Settings) {
	if challengeStart != config.Start || challengeEnd != config.End {
		if touchTimer != nil {
			touchTimer.Stop()
		}

		if config.Start.Unix() == 0 {
			log.Println("WARNING: No challenge start defined!")

			if _, err := os.Stat(startedFile); !os.IsNotExist(err) {
				os.Remove(startedFile)
			}

			return
		}

		startSub := time.Until(config.Start)
		if startSub > 0 {
			log.Println("Challenge will starts at", config.Start, "in", startSub)

			if _, err := os.Stat(startedFile); !os.IsNotExist(err) {
				os.Remove(startedFile)
			}

			touchTimer = time.AfterFunc(config.Start.Sub(time.Now().Add(time.Duration(1*time.Second))), touchStartedFile)
		} else {
			log.Println("Challenge started at", config.Start, "since", -startSub)
			touchStartedFile()
		}
		log.Println("Challenge ends on", config.End)

		challengeStart = config.Start
		challengeEnd = config.End
	} else {
		log.Println("Configuration reloaded, but start/end times doesn't change.")
	}

	enableResolutionRoute = config.EnableResolutionRoute
	denyNameChange = config.DenyNameChange
	acceptNewIssues = config.AcceptNewIssue
	allowRegistration = config.AllowRegistration
	enableResetProgression = config.WorkInProgress && config.CanResetProgression
}
