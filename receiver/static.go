package main

import (
	"net/http"
	"os"
	"path"
)

var staticDir = "static"

func serveIndex(w http.ResponseWriter, r *http.Request) {
	http.ServeFile(w, r, path.Join(staticDir, "index.html"))
}

type TeamMyServer struct {
	path2Dir string
}

func (s TeamMyServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if _, err := os.Stat(startedFile); os.IsNotExist(err) {
		http.ServeFile(w, r, path.Join(s.path2Dir, "wait.json"))
	} else {
		http.ServeFile(w, r, path.Join(s.path2Dir, "my.json"))
	}
}
