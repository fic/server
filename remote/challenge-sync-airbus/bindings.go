package main

import (
	"encoding/json"
	"io/ioutil"
	"os"

	"srs.epita.fr/fic-server/libfic"
)

type AirbusExercicesBindings map[int64]AirbusChallengeId

func ReadExercicesBindings(ebpath string) (AirbusExercicesBindings, error) {
	fd, err := os.Open(ebpath)
	if err != nil {
		return nil, err
	}
	defer fd.Close()

	jdec := json.NewDecoder(fd)

	var aeb AirbusExercicesBindings
	err = jdec.Decode(&aeb)

	return aeb, err
}

func getTeams(pathname string) (teams map[string]fic.ExportedTeam, err error) {
	var cnt_raw []byte
	if cnt_raw, err = ioutil.ReadFile(pathname); err != nil {
		return
	}

	if err = json.Unmarshal(cnt_raw, &teams); err != nil {
		return
	}

	return
}
