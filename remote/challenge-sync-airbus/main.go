package main

import (
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"syscall"
	"time"

	"gopkg.in/fsnotify.v1"
)

var (
	TeamsDir        string
	skipInitialSync bool
	dryRun          bool
)

func main() {
	flag.StringVar(&TeamsDir, "teams", "./TEAMS", "Base directory where save teams JSON files")
	var debugINotify = flag.Bool("debuginotify", false, "Show skipped inotofy events")
	flag.BoolVar(&dryRun, "dry-run", dryRun, "Don't perform any write action, just display")
	flag.BoolVar(&skipInitialSync, "skipinitialsync", skipInitialSync, "Skip the initial synchronization")
	flag.BoolVar(&noValidateChallenge, "no-validate-challenge", noValidateChallenge, "Consider challenge validation as a standard award (if each exercice hasn't been imported on their side)")
	skipVerify := flag.Bool("skip-tls-verify", false, "Allow not verified certificates (INSECURE!)")
	tsFromFile := flag.Bool("timestamp-from-file", false, "Load timestamp matching for score from filesystem instead of the API")
	daemon := flag.Bool("watch", false, "Enable daemon mode by watching the directory")
	tspath := flag.String("timestamp-file", "./REMOTE/timestamp", "Path to the file storing the last timestamp")
	exercicespath := flag.String("exercices-file", "./REMOTE/exercices-bindings.json", "Path to the file containing the ID bindings")
	coeff := flag.Float64("global-coeff", 10.0, "Coefficient to use to multiply all scores before passing them to the other platform")
	flag.Parse()

	api := AirbusAPI{
		BaseURL:            "https://portal.european-cybercup.lan/api",
		InsecureSkipVerify: *skipVerify,
	}

	if v, exists := os.LookupEnv("AIRBUS_BASEURL"); exists {
		api.BaseURL = v
	} else if v, exists := os.LookupEnv("AIRBUS_BASEURL_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open AIRBUS_BASEURL_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		api.BaseURL = strings.TrimSpace(string(b))

		fd.Close()
	}
	if v, exists := os.LookupEnv("AIRBUS_SKIP_TLS_VERIFY"); exists {
		var err error
		api.InsecureSkipVerify, err = strconv.ParseBool(v)
		if err != nil {
			log.Fatal("Unable to parse boolean value in AIRBUS_SKIP_TLS_VERIFY:", err)
		}
	}
	if v, exists := os.LookupEnv("AIRBUS_TOKEN"); exists {
		api.Token = v
	} else if v, exists := os.LookupEnv("AIRBUS_TOKEN_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open AIRBUS_TOKEN_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		api.Token = strings.TrimSpace(string(b))

		fd.Close()
	}

	if v, exists := os.LookupEnv("AIRBUS_SESSIONID"); exists {
		var err error
		api.SessionID, err = strconv.ParseInt(v, 10, 64)
		if err != nil {
			log.Fatal("AIRBUS_SESSIONID is invalid: ", err.Error())
		}
	} else if v, exists := os.LookupEnv("AIRBUS_SESSION_NAME_FILE"); exists {
		fd, err := os.Open(v)
		if err != nil {
			log.Fatal("Unable to open AIRBUS_SESSION_NAME_FILE:", err)
		}

		b, _ := ioutil.ReadAll(fd)
		fd.Close()

		v = strings.TrimSpace(string(b))

		sessions, err := api.GetSessions()
		if err != nil {
			log.Fatal("Unable to retrieve session: ", err)
		}

		for _, session := range sessions {
			if session.Name == v {
				api.SessionID = session.ID
				break
			}
		}

		if api.SessionID == 0 {
			log.Fatal("Session ID not found")
		} else {
			log.Println("Session ID discovered: ", api.SessionID)
		}
	} else if v, exists := os.LookupEnv("AIRBUS_SESSION_NAME"); exists {
		sessions, err := api.GetSessions()
		if err != nil {
			log.Fatal("Unable to retrieve session: ", err)
		}

		for _, session := range sessions {
			if session.Name == v {
				api.SessionID = session.ID
				break
			}
		}

		if api.SessionID == 0 {
			log.Fatal("Session ID not found")
		} else {
			log.Println("Session ID discovered: ", api.SessionID)
		}
	} else {
		sessions, err := api.GetSessions()
		if err != nil {
			log.Fatal("Unable to retrieve session (check your credentials!): ", err)
		}

		log.Println("Please define your AIRBUS_SESSIONID or AIRBUS_SESSION_NAME.")
		log.Println("Existing sessions are:")
		for _, session := range sessions {
			log.Printf(" - %d: %q", session.ID, session.Name)
		}
		os.Exit(1)
	}

	log.SetPrefix("[challenge-sync-airbus] ")

	if flag.NArg() > 0 {
		args := flag.Args()
		switch args[0] {
		case "list":
			teams, err := api.GetTeams()
			if err != nil {
				log.Println("Unable to retrieve teams:", err)
				os.Exit(1)
			}

			fmt.Println("## Airbus' registered teams:")
			fmt.Println("-------------------------------------------")
			fmt.Println(" ID |            Name | Nb. | Score | Rank")
			fmt.Println("-------------------------------------------")
			for _, team := range teams {
				fmt.Printf("% 2d | % 15s | % 3d | % 5d | % 3d\n", team.ID, team.Name, len(team.Members), team.Score, team.Rank)
			}
		case "rank":
			teams, err := api.GetTeams()
			if err != nil {
				log.Println("Unable to retrieve teams:", err)
				os.Exit(1)
			}

			ranking := []*AirbusTeam{}
			for _, team := range teams {
				tmp := team
				ranking = append(ranking, &tmp)
			}

			sort.Sort(sort.Reverse(ByScore(ranking)))

			fmt.Println("## Airbus' ranking:")
			fmt.Println("-------------------------------------")
			fmt.Println(" Rank |            Name | ID | Score")
			fmt.Println("-------------------------------------")
			for _, team := range ranking {
				fmt.Printf("% 5d | % 15s |% 3d | % 5d\n", team.Rank, team.Name, team.ID, team.Score)
			}
		case "get":
			teams, err := api.GetTeams()
			if err != nil {
				log.Println("Unable to retrieve teams:", err)
				os.Exit(1)
			}

			teamid, err := strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				log.Println(err)
				os.Exit(1)
			}

			for _, team := range teams {
				if team.ID == teamid {
					fmt.Printf("## Airbus' registered team %d:\n\nID: %d\nName: %s\nScore: %d\nRank: %d\nMembers:\n", teamid, team.ID, team.Name, team.Score, team.Rank)
					for _, member := range team.Members {
						fmt.Printf("  - ID: %d\n    Name: %s\n    Nickname: %s\n    E-mail: %s\n", member.ID, member.Name, member.Nickname, member.EMail)
					}
					os.Exit(0)
				}
			}
			fmt.Printf("Team %d not found. Use 'list' to view all existing teams\n", teamid)
		case "award":
			if len(args) < 3 {
				fmt.Println("award <TEAM_ID> <VALUE> <MESSAGE>")
				os.Exit(1)
			}

			teams, err := api.GetTeams()
			if err != nil {
				log.Println("Unable to retrieve teams:", err)
				os.Exit(1)
			}

			teamid, err := strconv.ParseInt(args[1], 10, 64)
			if err != nil {
				log.Println("Invalid team id", err)
				os.Exit(1)
			}

			value, err := strconv.ParseInt(args[2], 10, 64)
			if err != nil {
				log.Println("Invalid award value:", err)
				os.Exit(1)
			}

			for _, team := range teams {
				if team.ID == teamid {
					err = api.AwardUser(&team, value, strings.Join(args[3:], " "))
					if err != nil {
						log.Println("Unable to award team:", err)
						os.Exit(1)
					}

					fmt.Println("Team awarded")
					os.Exit(0)
				}
			}
			fmt.Printf("Team %d not found. Use 'list' to view all existing teams\n", teamid)
		}

		os.Exit(0)
	}

	var err error
	TeamsDir = path.Clean(TeamsDir)

	w := Walker{
		API:   api,
		Coeff: *coeff,
	}

	// Load teams.json
	w.Teams, err = getTeams(filepath.Join(TeamsDir, "teams.json"))
	if err != nil {
		log.Fatal("Unable to open teams bindings file: ", err.Error())
	}
	log.Println("Team bindings loaded: ", len(w.Teams))

	// Fetch teams from Airbus API
	if err = w.fetchTeams(); err != nil {
		log.Fatal("Unable to fetch Airbus teams: ", err.Error())
	}

	// Load the timestamp
	if *tsFromFile {
		w.LastSync, err = loadTS(*tspath)
	} else {
		w.LastSync, err = loadTSFromAPI(w.TeamBindings)
	}
	if err != nil {
		log.Fatal("Unable to open timestamp file: ", err.Error())
	}

	if !noValidateChallenge {
		// Load exercices bindings
		w.Exercices, err = ReadExercicesBindings(*exercicespath)
		if err != nil {
			log.Fatal("Unable to open exercices bindings file: ", err.Error())
		}
	}

	if !skipInitialSync {
		log.Println("Doing initial score balance")
		err = w.BalanceScores()
		if err != nil {
			log.Println("Something goes wrong during score balance: ", err.Error())
		}

		// save current timestamp for teams
		err = saveTS(*tspath, w.LastSync)
		if err != nil {
			log.Fatal("Unable to save timestamp file: ", err.Error())
		}

		log.Println("initial sync done")
	}

	if daemon != nil && *daemon {
		// Watch teams.json and scores.json
		log.Println("Registering directory events...")
		watcher, err := fsnotify.NewWatcher()
		if err != nil {
			log.Fatal(err)
		}
		defer watcher.Close()

		if err := watchsubdir(watcher, TeamsDir); err != nil {
			log.Fatal(err)
		}

		// Register SIGUSR1, SIGUSR2
		interrupt1 := make(chan os.Signal, 1)
		signal.Notify(interrupt1, syscall.SIGHUP)
		interrupt2 := make(chan os.Signal, 1)
		signal.Notify(interrupt2, syscall.SIGUSR1)
		interrupt3 := make(chan os.Signal, 1)
		signal.Notify(interrupt3, syscall.SIGUSR2)

		var ticker *time.Ticker
		if *tsFromFile {
			ticker = time.NewTicker(5 * time.Second)
		} else {
			ticker = time.NewTicker(5 * time.Minute)
		}

		watchedNotify := fsnotify.Create

		for {
			select {
			case <-interrupt1:
				log.Println("SIGHUP received, reloading files...")
				teamsbindings, err := getTeams(filepath.Join(TeamsDir, "teams.json"))
				if err != nil {
					log.Println("Unable to open teams bindings file: ", err.Error())
				} else {
					w.Teams = teamsbindings
				}

				if err = w.fetchTeams(); err != nil {
					log.Println("Unable to fetch teams: ", err.Error())
				}

				if !*tsFromFile {
					ts, err := loadTSFromAPI(w.TeamBindings)
					if err != nil {
						log.Println("Unable to refresh timestamp: ", err.Error())
					} else {
						w.LastSync = ts
					}
				}

				// save current timestamp for teams
				err = saveTS(*tspath, w.LastSync)
				if err != nil {
					log.Println("Unable to save timestamp file: ", err.Error())
				}
				log.Println("SIGHUP treated.")
			case <-interrupt2:
				log.Println("SIGUSR1 received, resynching all teams")
				// Iterate over teams scores
				err = filepath.WalkDir(TeamsDir, w.WalkScoreSync)
				if err != nil {
					log.Printf("Something goes wrong during walking")
				}

				// save current timestamp for teams
				err = saveTS(*tspath, w.LastSync)
				if err != nil {
					log.Println("Unable to save timestamp file: ", err.Error())
				}
				log.Println("SIGUSR1 treated.")
			case <-interrupt3:
				log.Println("SIGUSR2 received, resynching all teams from teams.json")
				teamsbindings, err := getTeams(filepath.Join(TeamsDir, "teams.json"))
				if err != nil {
					log.Println("Unable to open teams bindings file: ", err.Error())
					return
				} else {
					w.Teams = teamsbindings
				}

				if err = w.fetchTeams(); err != nil {
					log.Println("Unable to fetch teams: ", err.Error())
					return
				}

				if !*tsFromFile {
					ts, err := loadTSFromAPI(w.TeamBindings)
					if err != nil {
						log.Println("Unable to refresh timestamp: ", err.Error())
						return
					} else {
						w.LastSync = ts
					}
				}

				err = w.BalanceScores()
				if err != nil {
					log.Println("Unable to balance scores: ", err.Error())
				}

				log.Println("SIGUSR2 treated.")
			case ev := <-watcher.Events:
				d, err := os.Lstat(ev.Name)

				if err == nil && strings.HasPrefix(d.Name(), ".") {
					if *debugINotify {
						log.Println("Skipped event:", ev, "for", ev.Name)
					}
					continue
				}

				if err == nil && watchedNotify == fsnotify.Create && ev.Op&fsnotify.Write == fsnotify.Write {
					log.Println("FSNOTIFY WRITE SEEN. Prefer looking at them, as it appears files are not atomically moved.")
					watchedNotify = fsnotify.Write
				}

				if err == nil && ev.Op&fsnotify.Create == fsnotify.Create && d.Mode().IsDir() && d.Mode()&os.ModeSymlink == 0 {
					// Register new subdirectory
					if err := watchsubdir(watcher, ev.Name); err != nil {
						log.Println(err)
					}
				} else if err == nil && ev.Op&watchedNotify == watchedNotify && d.Mode().IsRegular() {
					if *debugINotify {
						log.Println("Treating event:", ev, "for", ev.Name)
					}

					if watchedNotify == fsnotify.Write {
						time.Sleep(100 * time.Millisecond)
					}

					if filepath.Base(ev.Name) == "scores.json" {
						go w.treat(ev.Name)
					} else if filepath.Base(ev.Name) == "teams.json" {
						teamsbindings, err := getTeams(filepath.Join(TeamsDir, "teams.json"))
						if err != nil {
							log.Println("Unable to open teams bindings file: ", err.Error())
						} else {
							w.Teams = teamsbindings
						}
						if err = w.fetchTeams(); err != nil {
							log.Println("Unable to fetch teams: ", err.Error())
						}
					}
				} else if err == nil && *debugINotify {
					log.Println("Skipped event:", ev, "for", ev.Name)
				}
			case err := <-watcher.Errors:
				log.Println("error:", err)
			case <-ticker.C:
				// save current timestamp for teams
				err = saveTS(*tspath, w.LastSync)
				if err != nil {
					log.Println("Unable to save timestamp file: ", err.Error())
				}
			}
		}
	}

	// save current timestamp for teams
	err = saveTS(*tspath, w.LastSync)
	if err != nil {
		log.Fatal("Unable to save timestamp file: ", err.Error())
	}
}

func watchsubdir(watcher *fsnotify.Watcher, pathname string) error {
	log.Println("Watch new directory:", pathname)
	if err := watcher.Add(pathname); err != nil {
		return err
	}

	if ds, err := ioutil.ReadDir(pathname); err != nil {
		return err
	} else {
		for _, d := range ds {
			p := path.Join(pathname, d.Name())
			if d.IsDir() && !strings.HasPrefix(d.Name(), ".") && d.Mode()&os.ModeSymlink == 0 {
				if err := watchsubdir(watcher, p); err != nil {
					return err
				}
			}
		}
		return nil
	}
}
