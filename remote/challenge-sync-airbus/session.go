package main

import ()

type Session struct {
	Name   string `json:"name"`
	Status string `json:"status"`
	ID     int64  `json:"id"`
	Mode   string `json:"mode"`
}

func (a *AirbusAPI) GetSessions() (ret []Session, err error) {
	err = a.request("GET", "/v1/sessions", nil, &ret)
	return
}
