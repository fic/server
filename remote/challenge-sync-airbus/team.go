package main

import (
	"fmt"
)

type AirbusTeam struct {
	ID      int64        `json:"id"`
	Members []TeamMember `json:"members"`
	Name    string       `json:"name"`
	Score   int64        `json:"score"`
	Rank    int          `json:"rank"`
}

type TeamMember struct {
	ID       int64  `json:"id"`
	Name     string `json:"name"`
	Nickname string `json:"nickname"`
	EMail    string `json:"email"`
}

type airbusDataTeam struct {
	Data []AirbusTeam `json:"data"`
}

func (a *AirbusAPI) GetTeams() ([]AirbusTeam, error) {
	var data airbusDataTeam
	err := a.request("GET", fmt.Sprintf("/v1/sessions/%d/teams", a.SessionID), nil, &data)
	if err != nil {
		return nil, err
	} else {
		return data.Data, nil
	}
}

type ByRank []*AirbusTeam

func (a ByRank) Len() int           { return len(a) }
func (a ByRank) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByRank) Less(i, j int) bool { return a[i].Rank < a[j].Rank }

type ByScore []*AirbusTeam

func (a ByScore) Len() int           { return len(a) }
func (a ByScore) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a ByScore) Less(i, j int) bool { return a[i].Score < a[j].Score }
