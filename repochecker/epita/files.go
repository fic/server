package main

import (
	"fmt"
	"path"
	"strings"

	"go.uber.org/multierr"

	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
)

func EPITACheckFile(file *fic.EFile, _ *fic.Exercice, exceptions *sync.CheckExceptions) (errs error) {
	// Enforce file format
	if path.Ext(file.Name) == ".rar" || path.Ext(file.Name) == ".7z" {
		errs = multierr.Append(errs, fmt.Errorf("this file use a forbidden archive type."))
	}

	// Check for stange file extension
	if strings.HasSuffix(file.Name, ".tar.zip") {
		errs = multierr.Append(errs, fmt.Errorf(".tar.zip is not a valid tar format"))
	}

	// Check .gz files have a dedicated hash
	if path.Ext(file.Name) == ".gz" && !strings.HasSuffix(file.Name, ".tar.gz") && len(file.ChecksumShown) == 0 {
		errs = multierr.Append(errs, fmt.Errorf("digest of original, uncompressed, file missing in DIGESTS.txt"))
	}

	// Check for huge file to compress
	if file.Size > 4000000 && path.Ext(file.Name) == ".tar" {
		errs = multierr.Append(errs, fmt.Errorf("archive to compress with bzip2"))
	} else if file.Size > 40000000 && (path.Ext(file.Name) == "" ||
		path.Ext(file.Name) == ".csv" ||
		path.Ext(file.Name) == ".dump" ||
		path.Ext(file.Name) == ".eml" ||
		path.Ext(file.Name) == ".json" ||
		path.Ext(file.Name) == ".log" ||
		path.Ext(file.Name) == ".mbox" ||
		path.Ext(file.Name) == ".pcap" ||
		path.Ext(file.Name) == ".pcapng" ||
		path.Ext(file.Name) == ".txt") {
		errs = multierr.Append(errs, fmt.Errorf("huge file to compress with gzip"))
	}

	return
}
