package main

import (
	"fmt"
	"strconv"
	"strings"
	"unicode"

	"go.uber.org/multierr"

	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
)

func EPITACheckKeyFlag(flag *fic.FlagKey, raw string, _ *fic.Exercice, exceptions *sync.CheckExceptions) (errs error) {
	if (flag.Label[0] == 'Q' || flag.Label[0] == 'q') && (flag.Label[1] == 'U' || flag.Label[1] == 'u') ||
		(flag.Label[0] == 'W' || flag.Label[0] == 'w') && (flag.Label[1] == 'H' || flag.Label[1] == 'h') {
		errs = multierr.Append(errs, fmt.Errorf("Label should not begin with %s. This seem to be a question. Reword your label as a description of the expected flag, `:` are automatically appended.", flag.Label[0:2]))
		flag.Label = flag.Label[1:]
	}

	label := []rune(flag.Label)
	if flag.Label[len(flag.Label)-1] != ')' && flag.Label[len(flag.Label)-1] != '©' && !unicode.IsLetter(label[len(label)-1]) && !unicode.IsDigit(label[len(label)-1]) {
		errs = multierr.Append(errs, fmt.Errorf("Label should not end with punct (%q). Reword your label as a description of the expected flag, `:` are automatically appended.", flag.Label[len(flag.Label)-1]))
	}

	if strings.HasPrefix(strings.ToLower(raw), "cve-") && flag.Type != "ucq" {
		errs = multierr.Append(errs, fmt.Errorf("CVE numbers are required to be UCQ with choice_cost"))
	}

	if _, err := strconv.ParseInt(raw, 10, 64); flag.Type == "key" && err == nil && !exceptions.HasException(":not-number-flag") {
		errs = multierr.Append(errs, fmt.Errorf("shouldn't be this flag a number type? (:not-number-flag)"))
	}

	if flag.Placeholder == "" && (strings.HasPrefix(flag.Type, "number") || flag.Type == "key" || flag.Type == "text" || (flag.Type == "ucq" && flag.ChoicesCost > 0)) {
		errs = multierr.Append(errs, fmt.Errorf("no placeholder defined"))
	}

	if strings.HasPrefix(flag.Type, "number") {
		min, max, step, err := fic.AnalyzeNumberFlag(flag.Type)
		if err != nil {
			errs = multierr.Append(errs, err)
		} else if min == nil || max == nil || step == nil {
			errs = multierr.Append(errs, fmt.Errorf("please define min and max for your number flag"))
		} else if (*max-*min) / *step <= 10 {
			errs = multierr.Append(errs, fmt.Errorf("to avoid bruteforce, define more than 10 possibilities"))
		}
	}

	return
}

func EPITACheckKeyFlagWithChoices(flag *fic.FlagKey, raw string, choices []*fic.FlagChoice, _ *fic.Exercice, exceptions *sync.CheckExceptions) (errs error) {
	if !exceptions.HasException(":bruteforcable-choices") {
		if len(choices) < 10 && flag.ChoicesCost == 0 {
			errs = multierr.Append(errs, fmt.Errorf("requires at least 10 choices to avoid brute-force"))
		} else if len(choices) < 6 && flag.ChoicesCost > 0 {
			errs = multierr.Append(errs, fmt.Errorf("requires at least 10 choices to avoid brute-force"))
		}
	}

	return
}
