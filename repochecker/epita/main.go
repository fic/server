package main

import (
	"srs.epita.fr/fic-server/admin/sync"
)

func RegisterChecksHooks(h *sync.CheckHooks) {
	h.RegisterFlagKeyHook(EPITACheckKeyFlag)
	h.RegisterFlagKeyWithChoicesHook(EPITACheckKeyFlagWithChoices)
	h.RegisterFileHook(EPITACheckFile)
}
