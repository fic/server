package main

import (
	"path/filepath"
	"strings"

	"go.uber.org/multierr"

	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
)

func InspectFile(file *fic.EFile, _ *fic.Exercice, exceptions *sync.CheckExceptions) (errs error) {
	if filepath.Ext(file.Name) == ".tar" || strings.HasSuffix(file.Name, ".tar.gz") || strings.HasSuffix(file.Name, ".tar.bz2") {
		// Check there is more than 1 file in tarball
		errs = multierr.Append(errs, checkTarball(file, exceptions))
	} else if filepath.Ext(file.Name) == ".zip" {
		// Check there is more than 1 file in zip
		errs = multierr.Append(errs, checkZip(file, exceptions))
	}

	return
}
