package main

import (
	"srs.epita.fr/fic-server/admin/sync"
)

var hooks *sync.CheckHooks

func RegisterChecksHooks(h *sync.CheckHooks) {
	hooks = h

	h.RegisterFileHook(InspectFile)
}
