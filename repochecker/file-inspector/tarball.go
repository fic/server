package main

import (
	"archive/tar"
	"compress/bzip2"
	"compress/gzip"
	"fmt"
	"io"
	"log"
	"strings"

	"go.uber.org/multierr"

	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
)

func checkTarball(file *fic.EFile, exceptions *sync.CheckExceptions) (errs error) {
	fd, closer, err := sync.GetFile(sync.GlobalImporter, file.GetOrigin())
	if err != nil {
		log.Printf("Unable to open %q: %s", file.GetOrigin(), err.Error())
		return
	}
	defer closer()

	var rd io.Reader
	if strings.HasSuffix(file.Name, ".tar.gz") {
		archive, err := gzip.NewReader(fd)
		if err != nil {
			log.Printf("Unable to uncompress gzip file %q: %s", file.Name, err.Error())
			return
		}
		defer archive.Close()
		rd = archive
	} else if strings.HasSuffix(file.Name, ".tar.bz2") {
		rd = bzip2.NewReader(fd)
	} else {
		rd = fd
	}

	nbFile := 0

	tarrd := tar.NewReader(rd)
	for {
		header, err := tarrd.Next()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Printf("An error occurs when analyzing the tarball %q: %s", file.Name, err.Error())
			return
		}

		info := header.FileInfo()
		if !info.IsDir() {
			nbFile += 1
		}
	}

	if nbFile < 2 {
		if !exceptions.HasException(":one-file-tarball") {
			errs = multierr.Append(errs, fmt.Errorf("don't make a tarball for one file"))
		}
	} else if nbFile < 5 && false {
		if !exceptions.HasException(":few-files-tarball") {
			errs = multierr.Append(errs, fmt.Errorf("don't make a tarball for so little files (:few-files-tarball)"))
		}
	} else {
		log.Printf("%d files found in %q", nbFile, file.Name)
	}

	return
}
