# PCAP-INSPECTOR

Inspects pcap and pcapng files for packets with ip src and ip dst using private IPs

Set `VERBOSE_PCAP_CHECK` environment variable to enable verbose mode

## Build library

go build -o pcap-inspector -buildmode=plugin .

## Requirement

github.com/google/gopacket

## TODO

Custom rules on packet filtering
Handle log files
