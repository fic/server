//go:build checkupdate
// +build checkupdate

package main

import (
	"encoding/json"
	"io"
	"log"
	"net/http"
)

const version = 12

func init() {
	go checkUpdate()
}

func checkUpdate() {
	res, err := http.Get("https://fic.srs.epita.fr/repochecker.version")
	if err == nil {
		defer res.Body.Close()
		dec := json.NewDecoder(res.Body)

		var v int
		if err := dec.Decode(&v); err == io.EOF || err == nil {
			if v > version {
				log.Println("Your repochecker version is outdated, please update it:\n    https://fic.srs.epita.fr/fic-binaries/")
			}
		}
	}
}
