package main

import (
	"fmt"

	"go.uber.org/multierr"

	"srs.epita.fr/fic-server/admin/sync"
	"srs.epita.fr/fic-server/libfic"
)

var hooks *sync.CheckHooks

func RegisterChecksHooks(h *sync.CheckHooks) {
	hooks = h

	h.RegisterExerciceHook(CheckResolutionVideo)
}

func CheckResolutionVideo(e *fic.Exercice, exceptions *sync.CheckExceptions) (errs error) {
	for _, err := range multierr.Errors(checkResolutionVideo(e, exceptions)) {
		errs = multierr.Append(errs, fmt.Errorf("resolution.mp4: %w", err))
	}

	return
}
