package settings

import (
	"encoding/json"
	"os"
	"strings"
)

// ChallengeFile is the expected name of the file containing the challenge infos.
const ChallengeFile = "challenge.json"

type ChallengePartner struct {
	Src  string `json:"img"`
	Alt  string `json:"alt,omitempty"`
	Href string `json:"href,omitempty"`
}

// ChallengeInfo stores common descriptions and informations about the challenge.
type ChallengeInfo struct {
	// Title is the displayed name of the challenge.
	Title string `json:"title"`
	// SubTitle is appended to the title.
	SubTitle string `json:"subtitle,omitempty"`
	// Authors is the group name of people making the challenge.
	Authors string `json:"authors"`
	// ExpectedDuration is the duration (in minutes) suggested when stating the challenge.
	ExpectedDuration uint `json:"duration"`
	// VideoLink is the link to explaination videos when the challenge is over.
	VideosLink string `json:"videoslink,omitempty"`

	// Description gives an overview of the challenge.
	Description string `json:"description,omitempty"`
	// Rules tell the player some help.
	Rules string `json:"rules,omitempty"`
	// YourMission is a small introduction to understand the goals.
	YourMission string `json:"your_mission,omitempty"`

	// MainLogo stores path to logos displayed in the header.
	MainLogo []string `json:"main_logo,omitempty"`
	// MainLink stores link to the parent website.
	MainLink string `json:"main_link,omitempty"`
	// DashboardBackground stores path to the background used on the public dashboard.
	DashboardBackground string `json:"dashboard_background,omitempty"`
	// Partners holds the challenge partners list.
	Partners []ChallengePartner `json:"partners,omitempty"`
}

// ReadChallenge parses the file at the given location.
func ReadChallengeInfo(content string) (*ChallengeInfo, error) {
	var s ChallengeInfo
	jdec := json.NewDecoder(strings.NewReader(content))

	if err := jdec.Decode(&s); err != nil {
		return &s, err
	}

	return &s, nil
}

// SaveChallenge saves challenge at the given location.
func SaveChallengeInfo(path string, s *ChallengeInfo) error {
	if fd, err := os.Create(path); err != nil {
		return err
	} else {
		defer fd.Close()
		jenc := json.NewEncoder(fd)

		if err := jenc.Encode(s); err != nil {
			return err
		}

		return nil
	}
}
