Bienvenue au challenge forensic 2019 de l'EPITA !

Commencez par vous connecter au réseau filaire afin d'obtenir une IP. Vous
n'aurez pas besoin d'être connecté au WiFi en parallèle pour accéder à
Internet.

Rendez-vous ensuite sur https://fic.srs.epita.fr/ pour commencer le challenge.

Bon courage !
