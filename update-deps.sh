#! /usr/bin/env nix-shell
#! nix-shell --pure -i bash -p go gitMinimal gnused nixUnstable

set -euxo pipefail

go mod tidy
git add go.sum go.mod

# e is extension of new sed that replace backreference in REPLACEMENT then execute the new script.
# it is a gnu extension but we use gnused so we are safe
# cf: https://unix.stackexchange.com/a/194229/246754
# originally I did sed -i -e 's#vendorSha256 = ".*";#vendorSha256 = "'"$(nix build .#fic-admin.go-modules 2>&1 | sed -ne 's/ *got: *//p')"'";#' flake.nix
# this has 2 draw back:
# - not idem potent the second run will output ""
# - it hide all other error message it was ridiculous since I omitted nixUnstable and got nix: command not found in bash subshell (set -e didn't helped)
# If one need to avoid gnu extension you might be able to swap stdout and stderr (cf https://stackoverflow.com/q/3618078/7227940) and pipe to sh
nix build .#fic-admin.go-modules 2>&1 | sed -e 's/ *got: *\(.*\)/sed -i -e "s#vendorSha256 = \\\".*\\\";#vendorSha256 = \\\"\1\\\";#" flake.nix/pe'
git add flake.nix
